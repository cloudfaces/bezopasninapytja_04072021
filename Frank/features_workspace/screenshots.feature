Feature: 
Create marketing screenshots

Scenario: TC 6: Create a card an propose it.
	When I change the locale to "de" and device "iPhone_Retina_4inch"
	Given There is an eybl card and the app is initialized
	Given there are images to pick with the image picker
	Given I launch the app
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Cards"
	Then I touch the "Specials" TabBar Item
   	Then I wait for 2 seconds
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Specials"	
	Then I touch the "Cards" TabBar Item
   	Then I wait for 2 seconds
    Then I open the card "eybl vorteilscard classico"
   	Then I wait for 5 seconds
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Barcode"	
	Then I touch the "Offers" TabBar Item
	Then I touch "Burton Umhängetasche"
   	Then I wait for nothing to be animating
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Offers"	
	Then I navigate back
	Then I touch the "Info" TabBar Item
   	Then I wait for nothing to be animating
	Then I touch the "Info" TabBar Item
   	Then I wait for nothing to be animating
	Then I touch "button_storefinder.png"
   	Then I wait for nothing to be animating
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Storefinder"	
	When I change the locale to "de" and device "iPhone_Retina_4inch"

Scenario: TC 7: Create a card an propose it.
	When I change the locale to "de" and device "iPhone_Retina_4inch"
	Given There is an eybl card and the app is initialized
	Given there are images to pick with the image picker
	Given I launch the app
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Cards"
	Then I touch the "Specials" TabBar Item
   	Then I wait for 2 seconds
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Specials"	
	Then I touch the "Cards" TabBar Item
   	Then I wait for 2 seconds
    Then I open the card "eybl vorteilscard classico"
   	Then I wait for 5 seconds
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Barcode"	
	Then I touch the "Offers" TabBar Item
	Then I touch "Burton Umhängetasche"
   	Then I wait for nothing to be animating
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Offers"	
	Then I navigate back
	Then I touch the "Info" TabBar Item
   	Then I wait for nothing to be animating
	Then I touch the "Info" TabBar Item
   	Then I wait for nothing to be animating
	Then I touch "button_storefinder.png"
   	Then I wait for nothing to be animating
	Then I save a screenshot for device "iPhone_Retina_4inch" and locale "de" with name "Storefinder"	




