//
//  AppDelegate+Configuration.h
//  mobile-pocket
//
//  Created by Simon Moser on 20.12.11.
//  Copyright (c) 2011 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Configuration) 

- (void) initRestKit;
- (void) initAppearance;
- (void) resetApp;

#ifdef FEATURE_CHOOSESERVERADDRESS
- (void) chooseServerAddress;
#endif

@end
