//
//  RecentListViewController.h
//  app
//
//  Created by Bernhard Kunnert on 08.05.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol AppDownloadViewControllerDelegate <NSObject>

@required
- (void) viewController:(id) viewController urlSelected:(NSString*) url;

@end

@interface RecentListViewController : BaseViewController

@property (weak, nonatomic) id<AppDownloadViewControllerDelegate> delegate;

@end
