//
//  AppDownloadViewHelper.h
//  app
//
//  Created by LeyLa on 5/12/17.
//  Copyright © 2017 CloudFaces. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDownloadViewController.h"
#import <WebKit/WebKit.h>


@class AppDownloadViewHelper;
@protocol AppDownloadViewHelperDelegate <NSObject>

- (void) reloadWithUrl: (NSString *) urlString;
- (void) removeLocalStorage;
@end

@interface AppDownloadViewHelper : NSObject <UITableViewDataSource, UITableViewDelegate, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>
@property(nonatomic, weak) id<AppDownloadViewHelperDelegate> delegate;

@property (nonatomic, strong) UITableView *tableView;

+ (AppDownloadViewHelper *)sharedInstance;
- (instancetype) init;

@property (strong, nonatomic) NSString *btnPosition;

@property (strong, nonatomic) NSString *overlayDebugSwitch;

@property (strong, nonatomic) NSString *errorsSwitch;
@property (strong, nonatomic) NSString *warningsSwitch;
@property (strong, nonatomic) NSString *infoSwitch;
@property (strong, nonatomic) NSString *debugSwitch;

@property (strong, nonatomic) NSString *showTimes;

//@property (strong, nonatomic) NSMutableDictionary* logDict;
@property (strong, nonatomic) NSMutableArray* logArray;
@property (strong, nonatomic) NSMutableArray* errorArray;
@property (strong, nonatomic) NSMutableArray* warnArray;
@property (strong, nonatomic) NSMutableDictionary* cpDict;
@property (strong, nonatomic) NSMutableArray* cpArr;

@property int forceCount;
@property int errorsCount;
@property int warnCount;
@property int infoCount;
@property int debugCount;

- (NSArray *) getLogArray;
- (void) openLogView;

@property (strong, nonatomic) UILabel * labelTime;
@property (strong, nonatomic) UILabel * labelMsg;
@property (strong, nonatomic) UIFont  *currentFont;
@property (nonatomic) CGFloat cellHeight;
@property (nonatomic) CGFloat labelOneWidth;
@property (nonatomic) CGFloat labelMsgWidth;

@property (strong, nonatomic) NSString *lastUrlString;
@property (strong, nonatomic) NSString *rootPage;
@property (strong, nonatomic) UIButton* showTimesbutton;

@end
