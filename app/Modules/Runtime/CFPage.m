//
//  CFPage.m
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//
#import "CFPage.h"
#import "CFPage+JSON.h"



@implementation CFPage

#pragma mark BSJsonSupported

- (id) proxyForJson {
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    [dict setObject:self.uniqueId forKey:@"uniqueId"];
    [dict setObject:self.type forKey:@"type"];
    if (self.title) {
        [dict setObject:self.title forKey:@"title"];
    }
    if (self.pageHeader) {
        [dict setObject:self.pageHeader forKey:@"pageHeader"];
    }
    return dict;
}

- (instancetype) initWithJson:(NSDictionary *)receivedObjects
{
    if ([super init]) {
        if (!receivedObjects) {
            return nil;
        }
        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];
        self.uniqueId = receivedObjects[@"uniqueId"];
        self.type = receivedObjects[@"type"];
        self.title = receivedObjects[@"title"];
        self.pageHeader = [[CFPageHeader alloc] initWithJson:receivedObjects[@"pageHeader"]];
    }
    return self;
}

@end
