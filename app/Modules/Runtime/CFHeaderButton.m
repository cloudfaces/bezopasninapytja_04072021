//
//  CFHeaderButton.m
//  app
//
//  Created by Bernhard Kunnert on 26.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFHeaderButton.h"
#import "CFPage+JSON.h"
#import "UIColor+Additions.h"
#import "UIImage+Asset.h"

@implementation CFHeaderButton

#pragma mark BSJsonSupported

- (id)proxyForJson
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    if (self.text) {
        [dict setObject:self.text forKey:@"text"];
    }
    if (self.textColor) {
        [dict setObject:[self.textColor hexString] forKey:@"textColor"];
    }
    if (self.iconName) {
        [dict setObject:self.iconName forKey:@"icon"];
    }
    if (self.function) {
        [dict setObject:self.function forKey:@"function"];
    }
    return dict;
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    if ([super init]) {
        if (!receivedObjects) {
            return nil;
        }
        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];

        self.text = receivedObjects[@"text"];
        self.textColor = [UIColor colorFromHexString:receivedObjects[@"textColor"]];
        self.iconName = receivedObjects[@"icon"];
        self.icon = [UIImage imageFromAsset:self.iconName];
        self.function = receivedObjects[@"function"];
    }
    return self;
}

@end
