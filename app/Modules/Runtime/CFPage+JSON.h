//
//  CFPage+JSON.h
//  app
//
//  Created by Bernhard Kunnert on 24.09.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFPage.h"
#import "TypedPage.h"


@interface CFPage (JSON)

+ (id<TypedPage>) fromJSON:(NSDictionary *)receivedObjects;
+ (NSDictionary*) removeNullObjectsFromDictionary:(NSDictionary*) source;

@end


