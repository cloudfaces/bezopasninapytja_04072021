//
//  CFPageHeader.m
//  app
//
//  Created by Bernhard Kunnert on 06.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFPageHeader.h"
#import "CFPage+JSON.h"
#import "UIColor+Additions.h"
#import "UIImage+Asset.h"

@implementation CFPageHeader

#pragma mark BSJsonSupported

- (instancetype) init
{
    if (self = [super init]) {
        self.statusBarColor = [UIColor blackColor];
    }
    return self;
}

- (id) proxyForJson
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    if (self.backgroundColor) {
        [dict setObject:[self.backgroundColor hexString] forKey:@"backgroundColor"];
    }
    if (self.titleColor) {
        [dict setObject:[self.titleColor hexString] forKey:@"titleColor"];
    }
    if (self.backgroundImageName) {
        [dict setObject:self.backgroundImageName forKey:@"backgroundImage"];
    }
    if (self.titleImageName) {
        [dict setObject:self.titleImageName forKey:@"titleImage"];
    }
    if (self.rightButtonsArray) {
        NSMutableArray* tabArray = [NSMutableArray arrayWithCapacity:self.rightButtonsArray.count];
        for (CFHeaderButton* rightButton in self.rightButtonsArray) {
           [tabArray addObject:[rightButton proxyForJson]];
        }
        [dict setObject:tabArray forKey:@"rightButton"];
    }
    if (self.leftButton) {
        [dict setObject:self.leftButton forKey:@"leftButton"];
    }
    if (self.contentView) {
        [dict setObject:self.contentView forKey:@"contentView"];
    }
    if (self.statusBarColor) {
        [dict setObject:[self.statusBarColor hexString] forKey:@"statusBarColor"];
    }
    return dict;
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    if (self = [self init]) {
        if (!receivedObjects) {
            return nil;
        }
        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];
        self.backgroundColor = [UIColor colorFromHexString:receivedObjects[@"backgroundColor"]];
        self.titleColor = [UIColor colorFromHexString:receivedObjects[@"titleColor"]];
        self.backgroundImageName = receivedObjects[@"backgroundImage"];
        self.titleImageName = receivedObjects[@"titleImage"];
        self.backgroundImage = [UIImage navBarImageFromAsset:self.backgroundImageName];
        self.titleImage = [UIImage imageFromAsset:self.titleImageName];
        NSArray* rightButtons = receivedObjects[@"rightButton"];
        NSMutableArray* rightButtonItems = [NSMutableArray arrayWithCapacity:rightButtons.count];
        
        for (NSDictionary* rightButtonDict in rightButtons) {
            CFHeaderButton* rightButton = [[CFHeaderButton alloc] initWithJson:rightButtonDict];
            [rightButtonItems addObject:rightButton];
        }
        self.rightButtonsArray = rightButtonItems;
        self.leftButton = [[CFHeaderButton alloc] initWithJson:receivedObjects[@"leftButton"]];
        self.contentView = [[CFContentView alloc] initWithJson:receivedObjects[@"contentView"]];
        self.statusBarColor = [UIColor colorFromHexString:receivedObjects[@"statusBarColor"]];
    }
    return self;
}

@end
