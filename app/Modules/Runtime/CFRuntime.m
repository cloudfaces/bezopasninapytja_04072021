//
//  CFRuntime.m
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFRuntime.h"
#import "UIImage+Asset.h"
#import <SSZipArchive.h>
#import "AppDownloadViewHelper.h"
#import "SlidingTabsHelper.h"

@interface CFRuntime ()

@property (strong, nonatomic) CFViewControllerFactory* viewControllerFactory;
@property(strong, nonatomic) DDXMLDocument* xmlDocument;
@property (strong, nonatomic) NSString* baseUrl;

@end

@implementation CFRuntime

@synthesize rightButtons;
@synthesize contentViewButton;

UILabel *warningsLabel;
UILabel *errorsLabel;
UIViewController* viewController;

- (id) init {
    self = [super init];
    if (self) {
        self.viewControllerFactory = [[CFViewControllerFactory alloc] init];
        self.webViewStack = [[NSMutableDictionary alloc] init];
    }

    return self;
}

+ (CFRuntime*) instance {
    static CFRuntime* _instance = nil;
    if (!_instance) {
        _instance = [[CFRuntime alloc] init];
    }
    return _instance;
}

- (BOOL) handleAppUpgrade
{
    NSString* currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString* lastVersion = [BundleSettings lastVersion];
    BOOL result = YES;
    if (![currentVersion isEqualToString:lastVersion]) {
        result = [self resetAppFiles];
        if (result) {
            [BundleSettings setLastVersion:currentVersion];
        }
    }
    return result;
}

- (BOOL) resetAppFiles
{
    BOOL result = YES;
    NSError* error = nil;
    NSFileManager* fileManager = [NSFileManager defaultManager];
    // clearing extracted dir
    [fileManager removeItemAtPath:[CFRuntime pathForExtractedApp] error:&error];
    [fileManager removeItemAtPath:[CFRuntime pathForExtractedLocalAppContent] error:&error];

    NSLog(@"Copying path %@ to %@", [CFRuntime pathForEmbeddedApp], [CFRuntime pathForExtractedApp]);
    result = result && [fileManager copyItemAtPath:[CFRuntime pathForEmbeddedApp] toPath:[CFRuntime pathForExtractedApp] error:&error];
    NSLog(@"Copying path %@ to %@", [CFRuntime pathForEmbeddedAppContent], [CFRuntime pathForExtractedLocalAppContent]);
    result = result && [fileManager copyItemAtPath:[CFRuntime pathForEmbeddedAppContent] toPath:[CFRuntime pathForExtractedLocalAppContent] error:&error];
    if (!result && error) {
        NSLog(@"Error occured while resetting App: %@", [error description]);
    }
    //To be discussed
    //[self setBackupFlags];
    return result;
}

- (void) setBackupFlags {
    [self setBackupFlagForUrl:[NSURL fileURLWithPath:[CFRuntime pathForExtractedApp]]];
    [self setBackupFlagForUrl:[NSURL fileURLWithPath:[CFRuntime pathForExtractedLocalAppContent]]];
    [self setBackupFlagForUrl:[NSURL fileURLWithPath:[CFRuntime pathForExtractedRemoteAppContent]]];
}

- (void) setBackupFlagForUrl:(NSURL*) url {
    if ([[NSFileManager defaultManager] fileExistsAtPath: [url path]]) {
        NSError *error = nil;
        BOOL success = [url setResourceValue: [NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            NSLog(@"Error excluding %@ from backup %@", [url lastPathComponent], error);
        } else {
            NSLog(@"SUCCESS excluding %@ from backup", [url lastPathComponent]);
        }
    } else {
        NSLog(@"Error excluding %@ from backup: Path does not exist", [url lastPathComponent]);
    }
}

- (BOOL) loadAppFromUrl:(NSURL*) url {
    NSError* error;

    // load and parse XML data
    NSData* data = [NSData dataWithContentsOfURL:url];
    self.xmlDocument = [[DDXMLDocument alloc] initWithData:data options:0 error:&error];
    if (error) {
        NSLog(@"Error while loading XML document: %@", error);
    } else {
        NSLog(@"Succeeded loading XML document");
    }

    self.baseUrl = [[url URLByDeletingLastPathComponent] absoluteString];
    BOOL unzipSuccess = YES;

    if ([BundleSettings isRemoteApp]) {
        // download and extract contens
        NSString* remoteContentFile = [self getRemoteContent];
        if (remoteContentFile) {
            NSURL *contentUrl = [NSURL URLWithString:[self.baseUrl stringByAppendingString:remoteContentFile]];
            NSData *contentData = [NSData dataWithContentsOfURL:contentUrl];
            NSString *tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"content.zip"];

            NSString *contentDataPath = [CFRuntime pathForExtractedRemoteAppContent];

            NSFileManager *fileManager = [NSFileManager defaultManager];

            [fileManager removeItemAtPath:tempFilePath error:NULL];
            [fileManager removeItemAtPath:contentDataPath error:NULL];
            unzipSuccess &= [contentData writeToFile:tempFilePath atomically:YES];
            unzipSuccess &= [SSZipArchive unzipFileAtPath:tempFilePath toDestination:contentDataPath];
        }
    }

    [self setBackupFlags];

    return (error == nil && unzipSuccess);
}

- (BOOL) downloadAndReplaceLocalFile:(NSString*) localPath withContentOfUrl:(NSURL*) remoteUrl
{
    NSError* error;
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSData* data = [NSData dataWithContentsOfURL:remoteUrl];
    if (!data) {
        return NO;
    }
    NSString* targetDir = [localPath stringByDeletingLastPathComponent];
    BOOL result = [fileManager createDirectoryAtPath:targetDir withIntermediateDirectories:YES attributes:nil error:&error];
    result = result && [data writeToFile:localPath options:NSDataWritingAtomic error:&error];
    return result;
}

- (NSString*) getRemoteContent
{
    NSError* error;
    NSArray* appNodes = [self.xmlDocument nodesForXPath:@"./App/@content_ios" error:&error];
    return (appNodes && appNodes.count > 0) ? [appNodes[0] stringValue] : nil;
}

- (NSString*) getRootPageId {
    NSError* error;
    NSArray* appNodes = [self.xmlDocument nodesForXPath:@"./App/@root" error:&error];
    return [appNodes[0] stringValue];
}

- (CFPage*) getRootPage {
    NSString* rootPageId = [self getRootPageId];
    CFPage* page = [self getPageWithId:rootPageId];
    
    return page;
}

- (CFPage*) getPageWithId:(NSString *)pageId {
    DDXMLElement* pageElement = [self getPageElement:pageId];
    if (pageElement) {
        CFPage* page = [[CFPage alloc] init];
        page.uniqueId = [[pageElement attributeForName:@"id"] stringValue];
        page.title = [[pageElement attributeForName:@"title"] stringValue];
        page.type = [[pageElement attributeForName:@"type"] stringValue];
        // detect header element
        NSError* error;
        NSArray* headerElements = [pageElement nodesForXPath:@"./pageheader" error:&error];
        if (!error && headerElements && headerElements.count > 0) {
            DDXMLElement* headerElement = headerElements[0];
            CFPageHeader* header = [[CFPageHeader alloc] init];
            NSString* attribute = [[headerElement attributeForName:@"backgroundcolor"] stringValue];
            if (![NSString isEmpty:attribute]) {
                header.backgroundColor = [UIColor colorFromHexString:attribute];
            }
            attribute = [[headerElement attributeForName:@"titlecolor"] stringValue];
            if (![NSString isEmpty:attribute]) {
                header.titleColor = [UIColor colorFromHexString:attribute];
            }
            attribute = [[headerElement attributeForName:@"titleimage"] stringValue];
            if (![NSString isEmpty:attribute]) {
                header.titleImageName = attribute;
                header.titleImage = [UIImage imageFromAsset:attribute];
            }
            attribute = [[headerElement attributeForName:@"statusbarcolor"] stringValue];
            if (![NSString isEmpty:attribute]) {
                header.statusBarColor = [UIColor colorFromHexString:attribute];
            }
            attribute = [[headerElement attributeForName:@"backgroundimage"] stringValue];
            if (![NSString isEmpty:attribute]) {
                NSString* fileName = attribute;
                UIImage* headerImage = [UIImage navBarImageFromAsset:fileName];
                header.backgroundImage = headerImage;
                header.backgroundImageName = attribute;
            }
            page.pageHeader = header;
            page.pageHeader.rightButtonsArray = [self findRightButtonFromHeader:headerElement withName:@"rightbutton"];
            page.pageHeader.leftButton = [self findButtonFromHeader:headerElement withName:@"leftbutton"];

            // detect custom content view elements
            page.pageHeader.contentView = [self findViewFromHeader:headerElement withName:@"contentview"];
        }
        return page;
    } else {
        return nil;
    }
}



- (CFContentView*) findViewFromHeader:(DDXMLElement*) headerElement withName:(NSString*) viewName
{
    NSError* error;
    NSArray* buttonElements = [headerElement nodesForXPath:[@"./" stringByAppendingString:viewName]  error:&error];
    if (!error && buttonElements && buttonElements.count > 0) {
        rightButtons = [[NSMutableArray alloc] init];
        CFContentView* contentView = [[CFContentView alloc] init];
        DDXMLElement* buttonElement = buttonElements[0];
        contentView.type = [[buttonElement attributeForName:@"type"] stringValue];
        NSString* attribute = [[buttonElement attributeForName:@"color"] stringValue];
        if (![NSString isEmpty:attribute]) {
            contentView.color = [UIColor colorFromHexString:attribute];
        }
        contentView.text =  [[buttonElement attributeForName:@"text"] stringValue];
        contentViewButton = contentView;
        return contentViewButton;
    }
    return nil;
}

- (NSString *) setContentViewButtonString: (NSString *) text {
    NSString *contentViewText;
    if ([text isEqual:@"date"]){
        //content view text
        NSDate *todayDate = [NSDate date]; // get today sdate
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter setDateFormat:@"dd MMM yyyy"]; //Here we can set the format which we need
        NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];// here convert date in
        contentViewText = convertedDateString;
    } else {

    }
    return contentViewText;
}

//rughtButtonArray
- (NSArray*) findRightButtonFromHeader:(DDXMLElement*) headerElement withName:(NSString*) buttonName
{
    NSError* error;
    NSArray* buttonElements = [headerElement nodesForXPath:[@"./" stringByAppendingString:buttonName]  error:&error];
    if (!error && buttonElements && buttonElements.count > 0) {
        rightButtons = [[NSMutableArray alloc] init];
        //   CFHeaderButton* button = [[CFHeaderButton alloc] init];
        
        for (DDXMLElement* buttonElement in buttonElements) {
            CFHeaderButton* button = [[CFHeaderButton alloc] init];
            button.text = [[buttonElement attributeForName:@"text"] stringValue];
            button.function = [[buttonElement attributeForName:@"function"] stringValue];
            NSString* attribute = [[buttonElement attributeForName:@"textcolor"] stringValue];
            if (![NSString isEmpty:attribute]) {
                button.textColor = [UIColor colorFromHexString:attribute];
            }
            attribute = [[buttonElement attributeForName:@"icon"] stringValue];
            if (![NSString isEmpty:attribute]) {
                button.iconName = attribute;
                button.icon = [UIImage imageFromAsset:attribute];
            }
            [rightButtons addObject:button];
        }
        return rightButtons;
    }
    return nil;
}

- (CFHeaderButton*) findButtonFromHeader:(DDXMLElement*) headerElement withName:(NSString*) buttonName
{
    NSError* error;
    NSArray* buttonElements = [headerElement nodesForXPath:[@"./" stringByAppendingString:buttonName]  error:&error];
    if (!error && buttonElements && buttonElements.count > 0) {
        rightButtons = [[NSMutableArray alloc] init];
        CFHeaderButton* button = [[CFHeaderButton alloc] init];
        NSLog(@"button %@", button);

        DDXMLElement* buttonElement = buttonElements[0];

        button.text = [[buttonElement attributeForName:@"text"] stringValue];
        button.function = [[buttonElement attributeForName:@"function"] stringValue];
        NSString* attribute = [[buttonElement attributeForName:@"textcolor"] stringValue];
        if (![NSString isEmpty:attribute]) {
            button.textColor = [UIColor colorFromHexString:attribute];
        }
        attribute = [[buttonElement attributeForName:@"icon"] stringValue];
        if (![NSString isEmpty:attribute]) {
            button.iconName = attribute;
            button.icon = [UIImage imageFromAsset:attribute];
        }
        return button;
    }
    return nil;
}

- (DDXMLElement*) getPageElement:(NSString *)pageId {
    NSError* error;
    NSArray* matchingPages = [self.xmlDocument nodesForXPath:[NSString stringWithFormat:@".//page[@id=\"%@\"]", pageId] error:&error];
    if (error || matchingPages.count == 0) {
        return nil;
    }
    DDXMLElement* pageElement = matchingPages[0];
    return pageElement;
}

+ (void) presentPage:(CFPage*) page
{
    UIViewController* rootController = ((UIWindow*) [[UIApplication sharedApplication].windows firstObject]).rootViewController;
    NSLog(@"rootController presentPage %@", rootController);

    if (rootController.presentedViewController != nil) {
        [rootController dismissViewControllerAnimated:YES completion:^{
            [CFRuntime presentPage:page];
        }];
    } else {
        CFPage* rootPage = page;
        UIViewController* viewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:rootPage];
        NSLog(@"viewController presentPage %@", viewController);
        UIViewController* container = [viewController isContainer] ? viewController : [[BaseNavigationController alloc] initWithRootViewController:viewController];
        NSLog(@"container presentPage %@", container);
        ((id<Navigation>) viewController).navigationService.page = rootPage;
        NSLog(@"navigationService presentPage %@", ((id<Navigation>) viewController).navigationService.page);
        [rootController presentViewController:container animated:NO completion:nil];
        NSLog(@"rootControllerrootController presentPage %@", rootController);
    }
}

+ (void) presentPage:(CFPage*) page withDebugoverlayInPosition:(NSString *) position {
    UIViewController* rootController = ((UIWindow*) [[UIApplication sharedApplication].windows firstObject]).rootViewController;
    if (rootController.presentedViewController != nil) {
        [rootController dismissViewControllerAnimated:YES completion:^{
            [CFRuntime presentPage:page withDebugoverlayInPosition:[AppDownloadViewHelper sharedInstance].btnPosition];
        }];
    } else {
        CFPage* rootPage = page;
        [AppDownloadViewHelper sharedInstance].rootPage = rootPage.uniqueId;
        viewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:rootPage];
        //debug overlay
        UIViewController * debugOverlay = [[UIViewController alloc] initWithNibName:nil bundle:nil];
        //top left
        if ([[AppDownloadViewHelper sharedInstance].btnPosition isEqual:@"topLeft"]) {
            debugOverlay.view.frame = CGRectMake(5, 70, 60, 50);
        }
        //top right
        if ([[AppDownloadViewHelper sharedInstance].btnPosition isEqual:@"topRight"]) {
            debugOverlay.view.frame = CGRectMake(rootController.view.bounds.size.width - 65, 70, 60, 50);
        }
        //bot left
        if ([[AppDownloadViewHelper sharedInstance].btnPosition isEqual:@"botLeft"]) {
            debugOverlay.view.frame = CGRectMake(5, rootController.view.bounds.size.height - 55, 60, 50);
        }
        //bot right
        if ([[AppDownloadViewHelper sharedInstance].btnPosition isEqual:@"botRight"]) {
            debugOverlay.view.frame = CGRectMake(rootController.view.bounds.size.width - 65, rootController.view.bounds.size.height - 55, 60, 50);
        }
        debugOverlay.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        debugOverlay.view.opaque = NO;
        debugOverlay.view.layer.borderWidth = 2.0f;

        UIImageView *imgview = [[UIImageView alloc]initWithFrame:CGRectMake(5, 0, 25, 50)];
        [imgview setImage:[UIImage imageNamed:@"spanner"]];
        [imgview setContentMode:UIViewContentModeScaleAspectFit];
        [debugOverlay.view addSubview:imgview];

        //add errors label
        errorsLabel = [[UILabel alloc] initWithFrame:CGRectMake(30 , 0 , 30 , 25)];
        errorsLabel.textColor = [UIColor colorFromHexString:@"#FF0000"];
        [errorsLabel setTextAlignment:NSTextAlignmentCenter];
        [errorsLabel setFont: [errorsLabel.font fontWithSize:14]];
        [errorsLabel setText:@"0"];
        errorsLabel.numberOfLines = 0;

        [debugOverlay.view addSubview:errorsLabel];

        //add warnings label
        warningsLabel = [[UILabel alloc] initWithFrame:CGRectMake(30 , 25 , 30 , 25)];
        warningsLabel.textColor = [UIColor colorFromHexString:@"#FFAA00"];
        [warningsLabel setTextAlignment:NSTextAlignmentCenter];
        [warningsLabel setFont: [warningsLabel.font fontWithSize:14]];

        [warningsLabel setText:@"0"];
        warningsLabel.numberOfLines = 0;
        [debugOverlay.view addSubview:warningsLabel];

        //add button
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];

        [button addTarget:self action: @selector(viewLogClicked:) forControlEvents: UIControlEventTouchUpInside];
        [debugOverlay.view addSubview:button];

        [viewController.view addSubview:debugOverlay.view];

        UIViewController* container = [viewController isContainer] ? viewController : [[BaseNavigationController alloc] initWithRootViewController:viewController];

        ((id<Navigation>) viewController).navigationService.page = rootPage;

        [rootController presentViewController:container animated:NO completion:nil];
    }
}

+ (NSString*) pathForExtractedApp
{
    NSArray *userPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [userPaths objectAtIndex:0]; // Get documents folder
    NSString *wwwDataPath = [documentsDirectory stringByAppendingPathComponent:@"www"];

    return wwwDataPath;
}

+ (NSString*) pathForExtractedAppContent
{
    return [BundleSettings isRemoteApp] ? [CFRuntime pathForExtractedRemoteAppContent] : [CFRuntime pathForExtractedLocalAppContent];
}

+ (NSString*) pathForExtractedLocalAppContent
{
    NSArray *userPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [userPaths objectAtIndex:0]; // Get documents folder
    NSString *wwwDataPath = [documentsDirectory stringByAppendingPathComponent:@"content"];
    return wwwDataPath;
}

+ (NSString*) pathForExtractedRemoteAppContent
{
    NSArray *userPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [userPaths objectAtIndex:0]; // Get documents folder
    NSString *wwwDataPath = [documentsDirectory stringByAppendingPathComponent:@"romote_content"];
    return wwwDataPath;
}

+ (NSString*) pathForAppFile:(NSString*) file ofType:(NSString*) type inDirectory:(NSString*) directory
{
    NSString* path = [CFRuntime pathForExtractedApp];
    if (directory) {
        path = [path stringByAppendingPathComponent:directory];
    }
    path = [path stringByAppendingPathComponent:file];
    if (type) {
        path = [path stringByAppendingPathExtension:type];
    }
    return path;
}

+ (NSString*) pathForEmbeddedApp
{
    return [[NSBundle mainBundle] pathForResource:@"www" ofType:nil ];
}

+ (NSString*) pathForEmbeddedAppContent
{
    return [[NSBundle mainBundle] pathForResource:@"content" ofType:nil ];
}

- (NSArray*) getLocalFilePaths
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSMutableArray* array = [NSMutableArray array];
    NSDirectoryEnumerator *resourcesEnum = [fileManager enumeratorAtPath:[CFRuntime pathForExtractedApp]];
    NSString* enumFile;
    while (enumFile = [resourcesEnum nextObject]) {
        [array addObject:[[CFRuntime pathForExtractedApp] stringByAppendingPathComponent:enumFile]];
    }

    resourcesEnum = [fileManager enumeratorAtPath:[CFRuntime pathForExtractedLocalAppContent]];
    while (enumFile = [resourcesEnum nextObject]) {
        [array addObject:[[CFRuntime pathForExtractedLocalAppContent] stringByAppendingPathComponent:enumFile]];
    }

    return array;
}


//Get Connection Type
- (NSString *) getConnectionType {
    NSString *connectionType = [[NSString alloc] init];

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    if (status == NotReachable) { //No internet
        connectionType = @"none";
    } else if (status == ReachableViaWiFi) { //WiFi
        connectionType = @"wifi";
    } else if (status == ReachableViaWWAN) { //3G
        connectionType = @"mobile";
    }
    return connectionType;
}

+ (void) viewLogClicked:(UIButton *)button {
    [[AppDownloadViewHelper sharedInstance] openLogView];
}

- (void) changeErrorLabel: (NSString *) errors {
    [errorsLabel setText:errors];
}

- (void) changeWarnLabel: (NSString *) warnings {
    [warningsLabel setText: warnings];
}

@end
