//
//  CFHttpResponse.m
//  app
//
//  Created by Bernhard Kunnert on 04.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//
#import "CFHttpResponse.h"

@implementation CFHttpResponse

+ (CFHttpResponse *)sharedInstance {
    static dispatch_once_t onceToken;
    static CFHttpResponse *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[CFHttpResponse alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(NSString *) getResponse {
    return _body;
}

@end
