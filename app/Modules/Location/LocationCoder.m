//
//  LocationCoder.m
//  app
//
//  Created by Bernhard Kunnert on 11.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <SBJson.h>
#import "LocationCoder.h"


@implementation LocationCoder

+ (NSDictionary*) dictionaryFromLocation:(CLLocation*) location
{
    NSDictionary* pos= @{
                         @"long" : [NSNumber numberWithDouble:location.coordinate.longitude],
                         @"lat" : [NSNumber numberWithDouble:location.coordinate.latitude]
                         };
    return pos;
}

+ (NSString*) stringFromLocation:(CLLocation*) location
{
    SBJsonWriter* json = [[SBJsonWriter alloc] init];
    
    return [json stringWithObject:[LocationCoder dictionaryFromLocation:location]];
}

+ (NSDictionary*) dictionaryFromCircularRegion:(CLCircularRegion*) region
{
    NSDictionary* fence= @{
                         @"long" : [NSNumber numberWithDouble:region.center.longitude],
                         @"lat" : [NSNumber numberWithDouble:region.center.latitude],
                         @"id" : region.identifier,
                         @"radius" : [NSNumber numberWithDouble:region.radius],
                         };
    return fence;
}

@end
