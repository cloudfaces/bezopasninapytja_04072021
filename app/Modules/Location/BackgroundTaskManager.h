//
//  BackgroundTaskManager.h
//  app
//
//  Created by LeyLa on 8/23/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//
#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>

@interface BackgroundTaskManager : NSObject

+(instancetype)sharedBackgroundTaskManager;

-(UIBackgroundTaskIdentifier)beginNewBackgroundTask;
-(void)endAllBackgroundTasks;

@end
