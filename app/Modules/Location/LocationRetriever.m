//
//  LocationRetriever.m
//  app
//
//  Created by Bernhard Kunnert on 07.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "LocationRetriever.h"
//#import "NativeBridge.h"



@interface LocationRetriever () <CLLocationManagerDelegate>


@property(nonatomic, strong) CLLocationManager* locationManager;
@property(nonatomic, strong) LocationUpdateBlock updateBlock;
@property(nonatomic, strong) PermissionStatus permissionStatus;
@property(nonatomic, strong) CheckPermission status;


@end
@implementation LocationRetriever
@synthesize delegate;

- (BOOL) isAuthorized:(CLAuthorizationStatus) status {
    return status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse;


}
- (void) requestPermission:(PermissionStatus) permissionStatus{
    self.permissionStatus = permissionStatus;
        [self.locationManager requestAlwaysAuthorization];
}

- (void) checkPermission:(CheckPermission) status{
    self.status = status;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    //Check permission 0 - determinated; 2 - do not allow; 4 - whenInUse; 3 - always.
    if (![self isAuthorized:[CLLocationManager authorizationStatus]] || [CLLocationManager authorizationStatus] == 0 || [CLLocationManager authorizationStatus] == 2 ){

        NSLog(@"[CLLocationManager authorizationStatus] %d ", [CLLocationManager authorizationStatus]);
        self.status(NO);

    }else{
        NSLog(@"[CLLocationManager authorizationStatus] %d ", [CLLocationManager authorizationStatus]);
        self.status(YES);
    }
}

- (void) updateCurrentLocation:(LocationUpdateBlock) updateBlock
{
    self.updateBlock = updateBlock;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self getLocation];
}

- (void) getLocation
{
    if (![self isAuthorized:[CLLocationManager authorizationStatus]]) {
        [self.locationManager requestAlwaysAuthorization];
    } else {
        [self.locationManager startUpdatingLocation];
    }
}

#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{

    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", self.locationManager.location.description);
    [self.locationManager stopUpdatingLocation];
    if (self.updateBlock) {
        self.updateBlock(self.locationManager.location);
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
      if (self.updateLocation) {
            [self.locationManager startUpdatingLocation];
      } else {
    _authorizationStatus = @"";
    //Status: whenInUse  = 4; alwaysAllow = 3; doNotAllow = 2;
    //set status
    if (status == 4) {
        _authorizationStatus = @"whenInUse";
    }
    if (status == 3) {
        _authorizationStatus = @"alwaysAllow";
    }
    if (status == 2) {
        _authorizationStatus = @"doNotAllow";
    }

   if ((status != 0) && (![_authorizationStatus isEqualToString:@""]) ) {

       if (self.permissionStatus) {
           self.permissionStatus(_authorizationStatus);
       }
    }
    }

}



@end
