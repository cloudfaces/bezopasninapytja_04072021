//
//  LocationFencer.m
//  app
//
//  Created by Bernhard Kunnert on 11.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "LocationFencer.h"
#import "CFHttpRequest.h"
#import "LocationCoder.h"
#import <SBJson.h>

@interface LocationFencer () <CLLocationManagerDelegate>

@property(nonatomic, strong) CLLocationManager* locationManager;
@property(nonatomic, copy) NSString* url;

@property(nonatomic, strong) NSMutableArray* nonActivatedRegions;

@end

@implementation LocationFencer

@dynamic monitoredRegions;

+ (instancetype) instance {
    static LocationFencer* retriever = nil;
    if (!retriever) {
        retriever = [[LocationFencer alloc] initInternal];
    }
    return retriever;
}

- (instancetype) init {
    [NSException raise:@"Invalid" format:@"use instance to create instance of this type"];
    return nil;
}

- (instancetype) initInternal {
    self = [super init];
    if (self) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.nonActivatedRegions = [[NSMutableArray alloc] init];
    }
    return self;
}

- (BOOL) isAuthorized:(CLAuthorizationStatus) status {
    return status == kCLAuthorizationStatusAuthorizedAlways;
}

- (void) sendLocationToServer:(CLLocation*) location
{
    CFHttpRequest* request = [[CFHttpRequest alloc] init];
    request.url = self.url;
    request.method = @"POST";
    request.body = [LocationCoder stringFromLocation:location];
    
    [request send];
}

- (void) addObservedRegion:(CLRegion*) region {
    if (![self isAuthorized:[CLLocationManager authorizationStatus]]) {
        [self.nonActivatedRegions addObject:region];
        [self.locationManager requestAlwaysAuthorization];
    } else {
        [self.locationManager startMonitoringForRegion:region];
    }
}

- (void) removeObservedRegion:(NSString*) regionName
{
    for (CLRegion* region in self.locationManager.monitoredRegions) {
        if ([region.identifier isEqualToString:regionName]) {
            [self.locationManager stopMonitoringForRegion:region];
        }
    }}

- (NSSet*) monitoredRegions {
    return self.locationManager.monitoredRegions;
}

- (void) sendEvent:(NSString*) event toServer:(CLCircularRegion*) region
{
    NSString* url = [BundleSettings fencingUrl];
    if (![NSString isEmpty:url]) {
        NSDictionary* data = @{@"event" : event,
                               @"fence" : [LocationCoder dictionaryFromCircularRegion:region]};
        
        
        CFHttpRequest* request = [[CFHttpRequest alloc] init];
        request.url = url;
        request.method = @"POST";
        
        SBJsonWriter* json = [[SBJsonWriter alloc] init];
        
        request.body = [json stringWithObject:data];
        
        [request send];
    }
}

/*
- (void) sendLocalNotification
{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    
    localNotif.alertBody = @"Body";
    localNotif.alertTitle = @"Title";
    localNotif.soundName = UILocalNotificationDefaultSoundName;

    NSDictionary *infoDict = @{@"cfpushaction": @{@"type" : @"myTag"}};
    localNotif.userInfo = infoDict;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
}*/

#pragma mark CLLocationManagerDelegate




/*
 *  locationManager:didEnterRegion:
 *
 *  Discussion:
 *    Invoked when the user enters a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 */
- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region
{
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GEO_FENCE_ENTRY object:region];
        [self sendEvent:@"enter" toServer:(CLCircularRegion*) region];
    }
    
}

/*
 *  locationManager:didExitRegion:
 *
 *  Discussion:
 *    Invoked when the user exits a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 */
- (void)locationManager:(CLLocationManager *)manager
          didExitRegion:(CLRegion *)region
{
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GEO_FENCE_EXIT object:region];
        [self sendEvent:@"exit" toServer:(CLCircularRegion*) region];
    }
}

/*
 *  locationManager:monitoringDidFailForRegion:withError:
 *
 *  Discussion:
 *    Invoked when a region monitoring error has occurred. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
monitoringDidFailForRegion:(CLRegion *)region
              withError:(NSError *)error
{
    
}


/*
 *  locationManager:didStartMonitoringForRegion:
 *
 *  Discussion:
 *    Invoked when a monitoring for a region started successfully.
 */
- (void)locationManager:(CLLocationManager *)manager
didStartMonitoringForRegion:(CLRegion *)region
{
    
}



- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ([self isAuthorized:[CLLocationManager authorizationStatus]]) {
        NSArray* regionsToActivate = [self.nonActivatedRegions copy];
        for (CLRegion* region in regionsToActivate) {
            [self.locationManager startMonitoringForRegion:region];
            [self.nonActivatedRegions removeObject:region];
        }
    }
}

@end
