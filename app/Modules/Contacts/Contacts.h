//
//  Contacts.h
//  app
//
//  Created by LeyLa Mehmed on 1/15/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ContactsUI/ContactsUI.h>

@interface Contacts : NSObject
@property (strong, nonatomic) NSMutableArray* contacts;
@property (strong, nonatomic) NSMutableArray* contactDetails;
@property (strong, nonatomic) NSMutableArray* addrArr;

@property (strong, nonatomic) NSString *raw_id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *note;

@property CNContactStore *store;
+ (Contacts *) sharedInstance;
- (instancetype) init;

- (void) contactScan;
- (void) getContacts;
- (void) getContactDetails :(NSString*) contactsId;



@end
