//
//  CFIdentity.h
//  app
//
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFIdentity : NSObject

+ (NSString*)advertisingIdentifier;

@end
