//
//  CFShare.m
//  app
//
//  Created by Jakob Haider on 15/09/15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFShare.h"



@implementation CFShare

+ (void)shareString:(NSString *)text parentViewController:(UIViewController*)parent
{
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[text] applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint];
    
    [parent presentViewController:activityVC animated:YES completion:nil];
}

@end
