//
//  CFShare.h
//  app
//
//  Created by Jakob Haider on 15/09/15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFShare : NSObject

+ (void)shareString:(NSString *)text parentViewController:(UIViewController*)parent;

@end
