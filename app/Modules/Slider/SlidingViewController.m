//
//  SlidingViewController.m
//  app
//
//  Created by Bernhard Kunnert on 05.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "SlidingViewController.h"
#import "CFRuntime.h"
#import "CFSliderPage.h"
#import "DefaultNavigationImpl.h"
#import "BadgeBarButtonItem.h"
#import "Badges.h"
#import "AppDelegate.h"
#import "MenuPositionHelper.h"

@interface SlidingViewController () <NavigationService>
@property Badges *badgesContext;

@property (strong, nonatomic) NSString *badgeNumber;

@property (strong, nonatomic) NSString *slideMenu;
@property (strong, nonatomic) NSString *tapMenu;
@property (strong, nonatomic) NSString *directionMenu;

@property (strong, nonatomic) BaseNavigationController* menuContainer;

@property (strong, nonatomic) NSString *menuViewPosition;

@end

@implementation SlidingViewController


@dynamic navigationService;
@synthesize navigationContext = _navigationContext;
@synthesize page = _page;
@synthesize delegate = _delegate;
@synthesize typedPage = _typedPage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    return self;
}

- (void)viewDidLoad
{
    if (self.slideMenu && [self.slideMenu isEqual:@"true"]) {
        [self panGestureRecognizer];
    }
    if (self.tapMenu && [self.tapMenu isEqual:@"true"]) {
        [self tapGestureRecognizer];
    }
    
    [self updateAppearance];
    [super viewDidLoad];
    
    self.badgesContext = [Badges sharedInstance];
    _badgeNumber = [self.badgesContext getBadgeNumber];
    
    if (!([_badgeNumber isEqualToString:@"0"])) {
        CFSliderPage *sliderPage = [[CFSliderPage alloc]init];
        [sliderPage menuPage];
        
        CFPage *page3 =  [sliderPage menuPage];
        [self setPage:page3];
        [self navigateToMenuPage:page3 withContext:_navigationContext];
    }
}

- (void) reloadApp {
    [self viewDidLoad];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) isContainer
{
    return YES;
}

#pragma mark Navigation

- (void) applyHeaderAttributesOnNavigationController:(UINavigationController*) navigationController andButtonTarget:(id<ButtonTarget>) target
{
    [DefaultNavigationImpl clearPageHeaderAttributes:navigationController];
    
    BOOL shouldApplyOwnHeader = YES;
    
    // Do not apply own header if a subview has hidden the tab bar for navigating to one of their "children".
    if ([self.frontViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* container = (UINavigationController*) self.frontViewController;
        
        shouldApplyOwnHeader = container.viewControllers.count <= 1;
    }
    
    if (shouldApplyOwnHeader) {
        [DefaultNavigationImpl applyPageHeaderAttributes:self.navigationService.page.pageHeader toNavigationController:navigationController andButtonTarget:target];
    }
}

- (id<NavigationService>) navigationService {
    return self;
}

#pragma mark NavigationService (MenuNavigation)

- (void) setPage:(CFPage *)page
{
    BOOL reloadSubPages = !_page;
    
    if (_page != page) {
        _page = page;
        _page2 = _page;
        if (_page && reloadSubPages) {
            CFSliderPage* sliderPage = [[CFSliderPage alloc] initWithPage:_page];
            self.typedPage = sliderPage;
            
            UIViewController<Navigation>* menuViewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:sliderPage.menuPage];
            menuViewController.navigationService.page = sliderPage.menuPage;
            menuViewController.navigationService.navigationContext = self.navigationContext;
            
            UIViewController<Navigation>* contentViewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:sliderPage.contentPage];
            contentViewController.navigationService.page = sliderPage.contentPage;
            contentViewController.navigationService.navigationContext = self.navigationContext;
            
            self.badgesContext = [Badges sharedInstance];
            _badgeNumber = [self.badgesContext getBadgeNumber];
            
            self.slideMenu = sliderPage.slide;
            self.tapMenu = sliderPage.tap;
            self.directionMenu = sliderPage.direction;
            
            UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            
            // Add your action to your button
            if (([[MenuPositionHelper sharedInstance].menuPosition isEqual:@"right"]) || [self.directionMenu isEqual:@"right"]) {
                [customButton addTarget:self action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                [customButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            }


            // Customize your button as you want, with an image if you have a pictogram to display for exampleself.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer());

            [customButton setImage:[UIImage imageNamed:@"reveal-icon"] forState:UIControlStateNormal];
           // [customButton addGestureRecognizer:self.revealViewController.panGestureRecognizer];
           // [customButton addGestureRecognizer:[self panGestureRecognizer]];
            // Then create and add our custom BadgeBarButtonItem
            BadgeBarButtonItem *barButton = [[BadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
            // Set a value for the badge
            barButton.badgeValue = _badgeNumber;
            barButton.badgeOriginX = 13;
            barButton.badgeOriginY = -9;
            
            // Add it as the leftBarButtonItem of the navigation bar
            contentViewController.navigationItem.leftBarButtonItem = barButton;
            
            BaseNavigationController* contentContainer = [[BaseNavigationController alloc] initWithRootViewController:contentViewController];
            _menuContainer = [[BaseNavigationController alloc] initWithRootViewController:menuViewController];
            
            self.frontViewController = contentContainer;
            if ([[MenuPositionHelper sharedInstance].menuPosition isEqual:@"right"]) {
                self.frontViewController = contentContainer;
                self.rearViewController = nil;
                self.rightViewController = _menuContainer;
            } else {
                self.frontViewController = contentContainer;
                self.rightViewController = nil;
                self.rearViewController = _menuContainer;
            }

            self.title = contentViewController.title;
        }
    }
}

- (void) updateAppearance
{
    CFSliderPage* sliderPage = (CFSliderPage*) self.typedPage;
    if (!sliderPage) {
        sliderPage = [[CFSliderPage alloc] initWithPage:_page];
        self.typedPage = sliderPage;
    }
}

- (void) navigateToMenuPage:(CFPage *)page withContext:(id) navigationContext
{
    // move in content page
    UIViewController<Navigation>* viewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:page];
    
    viewController.navigationService.navigationContext = navigationContext;
    viewController.navigationService.delegate = nil; // no back naviagtion = no result
    viewController.navigationService.page = page;
    
    self.badgesContext = [Badges sharedInstance];
    _badgeNumber = [self.badgesContext getBadgeNumber];
    
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    // Add your action to your button
//    [customButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];

    if ([[MenuPositionHelper sharedInstance].menuPosition isEqual:@"right"]) {
        [customButton addTarget:self action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
        self.rearViewController = nil;
        self.rightViewController = _menuContainer;
    } else {
        [customButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        self.rightViewController = nil;
        self.rearViewController = _menuContainer;
    }

    // Customize your button as you want, with an image if you have a pictogram to display for example
    [customButton setImage:[UIImage imageNamed:@"reveal-icon"] forState:UIControlStateNormal];
    
    // Then create and add our custom BadgeBarButtonItem
    BadgeBarButtonItem *barButton = [[BadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    // Set a value for the badge
    barButton.badgeValue = _badgeNumber;
    barButton.badgeOriginX = 13;
    barButton.badgeOriginY = -9;
    
    // Add it as the leftBarButtonItem of the navigation bar
    viewController.navigationItem.leftBarButtonItem = barButton;
    
    BaseNavigationController* container = [[BaseNavigationController alloc] initWithRootViewController:viewController];
    
    self.frontViewController = container;
    [self setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

- (void) revealToggle:(id)sender
{
    [super revealToggle:sender];
    
    UIViewController* topViewController = self.frontViewController;
    if ([topViewController isKindOfClass:[UIViewController class]]) {
        topViewController = [((UINavigationController*) topViewController) topViewController];
    }
    topViewController.view.userInteractionEnabled = self.frontViewPosition == FrontViewPositionLeft;
}

- (void) rightRevealToggle:(id)sender
{
    [super rightRevealToggle:sender];

    UIViewController* topViewController = self.frontViewController;
    if ([topViewController isKindOfClass:[UIViewController class]]) {
        topViewController = [((UINavigationController*) topViewController) topViewController];
    }
    topViewController.view.userInteractionEnabled = self.frontViewPosition == FrontViewPositionRight;
}

- (void) navigateToPage:(CFPage *)page withContext:(id)navigationContext
{
    // not possible
}

- (void) navigateToPageAndAddToStack:(CFPage *)page withContext:(id)navigationContext
{
    // not possible
}

- (void) navigateBackWithResult:(id) result
{
    // not possible. Menu navigation does not have a back stack
}

- (void) navigateBackWithResult:(id)result onStackView:(NSString *) pageId 
{
    // not possible. Menu navigation does not have a back stack
}

- (void) navigateBackWithResultToRoot:(id)result onStackView:(NSString *) pageId{
}

- (void) navigateToRoot {
}

@end
