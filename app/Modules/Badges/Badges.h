//
//  Badges.h
//  app
//
//  Created by LeyLa Mehmed on 1/24/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Badges : NSObject

+ (Badges *)sharedInstance;
- (instancetype) init;

- (void) getBadge;
- (NSString *) updateBadgeNumber:(NSString *) badgeNumber;
- (NSString *) getBadgeNumber;
- (NSString *) setData:(NSString *) data;

@property long int badgeNum;
@end
