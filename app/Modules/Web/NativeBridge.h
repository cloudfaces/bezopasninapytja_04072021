//
//  NativeBridge.h
//  app
//
//  Created by Bernhard Kunnert on 06.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WKWebView.h>

@class NativeBridge;

@protocol NativeBridgeDelegate <NSObject>

- (void) nativeBridge:(NativeBridge*) bridge handleCall:(NSString*) functionName withArguments:(NSArray*) arguments callbackId:(NSInteger) callbackId forWebView:(WKWebView*) webView;

@end

@interface NativeBridge : NSObject
+ (NativeBridge*) instance;

- (void)returnResult:(NSInteger)callbackId synchronously:(BOOL) synchronous args:(id)arg, ...;
- (id) invoke:(NSString*) function waitUntilDone:(BOOL) wait args:(id)arg, ...;

@property (weak, nonatomic) IBOutlet WKWebView* webView;
@property (weak, nonatomic) IBOutlet id<NativeBridgeDelegate> delegate;

@end
