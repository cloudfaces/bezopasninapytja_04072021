//
//  CFHtmlPage.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFHtmlPage.h"
#import "CFRuntime.h"

@interface CFHtmlPage ()

@property (strong, nonatomic, readwrite) CFPage* page;

@end

@implementation CFHtmlPage

@dynamic source;
@dynamic backgroundColor;
@dynamic fullScreen;

#pragma mark BSJsonSupported

- (id)proxyForJson
{
    return [self.page proxyForJson];
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    self = [self initWithPage:[[CFPage alloc] initWithJson:receivedObjects]];
    return self;
}

- (id) initWithPage:(CFPage *)page {
    self = [super init];
    if (self) {
        self.page = page;
    }
    return self;
}

- (NSString*) source {
    NSError* error;
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
    NSString* source = [[pageElement nodesForXPath:@"./webview/@source" error:&error][0] stringValue];
    return source;
}

- (UIColor*) backgroundColor {
    NSError* error;
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
    NSArray* elements = [pageElement nodesForXPath:@"./webview/@backgroundcolor" error:&error];
    if (elements && elements.count > 0 && !error) {
        NSString* backgroundColor = [elements[0] stringValue];
        if (![NSString isEmpty:backgroundColor]) {
            return [UIColor colorFromHexString:backgroundColor];
        }
    }
    return nil;
}

- (BOOL) fullScreen {
    NSError* error;
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
    NSArray* elements = [pageElement nodesForXPath:@"./webview/@fullscreen" error:&error];
    if (elements && elements.count > 0 && !error) {
        BOOL fullscreen = [[elements[0] stringValue] boolValue];
        return fullscreen;
    }
    return NO;
}

@end
