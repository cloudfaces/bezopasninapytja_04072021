//
//  NativeBridge.m
//  app
//
//  Created by Bernhard Kunnert on 06.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "NativeBridge.h"
#import <SBJson.h>
#import <WebKit/WebKit.h>



@interface WKWebView(SynchronousEvaluateJavaScript)
- (NSString *)stringByEvaluatingJavaScriptFromString:(NSString *)script;
@end

@implementation WKWebView(SynchronousEvaluateJavaScript)
- (NSString *) stringByEvaluatingJavaScriptFromString:(NSString *) script
{
//    NSLog(@"Pavel - NativeBridge.m, returnResult, evaluateJavaScript - %@ \n", script);
    
    __block NSString *resultString = nil;
    __block BOOL finished = NO;
    
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//    dispatch_async(dispatch_get_main_queue(), ^{ // Incorrect Behavior > result is returned before the resultString itself
        
        [self evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        //[self evaluateJavaScript:[script stringByAppendingString:@" tralala(;"] completionHandler:^(id result, NSError *error) {
//            NSLog(@"Pavel - NativeBridge.m, returnResult, evaluateJavaScript completionHandler - script: %@ \n", script);
            
            if (error == nil) {
                if (result != nil) {
                    resultString = [NSString stringWithFormat : @"%@", result];
//                    NSLog(@"Pavel - NativeBridge.m, returnResult, evaluateJavaScript result: %@ \n", resultString);
                }
            } else {
//                NSLog(@"Pavel - NativeBridge.m, returnResult, evaluateJavaScript error: %@, result: %@ \n", error.localizedDescription, result);
            }
            finished = YES;
        }];
        
        NSInteger cs = 0;
        while (!finished) {
            cs++;
//            NSLog(@"Pavel - NativeBridge.m, returnResult, step: %ld \n", (long)cs);
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            //[[NSRunLoop currentRunLoop] runMode:NSRunLoopCommonModes beforeDate:[NSDate distantFuture]];
            //NSLog(@"Pavel - NativeBridge.m, returnResult, after loop step\n\n\n");
        }
    });

//    NSLog(@"Pavel - NativeBridge.m, returnResult, return result: %@, script: %@, finished: %@ \n", resultString, script, finished?@"YES":@"NO");

    return resultString;
}
@end


@interface NativeBridge () <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>
{
    
}

//@property (nonatomic, strong) void (^ completionHandler)(NSString *);

@end



@implementation NativeBridge

+ (NativeBridge*) instance {
//    NSLog(@"Pavel - NativeBridge.m, instance");
    
    static NativeBridge* _instance = nil;
    if (!_instance) {
        _instance = [[NativeBridge alloc] init];
    }
    return _instance;
}

- (void) setWebView:(WKWebView *)webView {
//    NSLog(@"Pavel - NativeBridge.m, setWebView");
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    [[config userContentController] addScriptMessageHandler:self name: @"log"];
    //WKWebView *webview = [[WKWebView alloc] initWithFrame:[self.webView frame] configuration:config];
    
    _webView.UIDelegate = self;
    if (_webView != webView) {
        if (_webView) {
            _webView.navigationDelegate = nil;
        }
        _webView = webView;
        if (_webView) {
            _webView.navigationDelegate = self;
        }
    }
}



- (void) dealloc {
//    NSLog(@"Pavel - NativeBridge.m, dealloc");
    
    self.webView.navigationDelegate = nil;
}

- (void)handleCall:(NSString*)functionName callbackId:(NSNumber*) callbackId args:(NSArray*) args
{
//    NSLog(@"Pavel - NativeBridge.m, handleCall %@, callbackId %@"
//          , functionName
//          , callbackId);
    
    if (self.delegate) {
        [self.delegate nativeBridge:self handleCall:functionName withArguments:args callbackId:[callbackId integerValue] forWebView:self.webView];
//        NSLog(@"Pavel - NativeBridge.m, handleCall, function: %@, callback: %ld", functionName, (long)[callbackId integerValue]);
    } else {
//        NSLog(@"Pavel - NativeBridge.m, handleCall no self.delegate");
    }
}

- (void)returnResult:(NSInteger)callbackId synchronously:(BOOL) synchronous args:(id)arg, ...;
{
//    NSLog(@"Pavel - NativeBridge.m, returnResult: %ld, synchronous: %@, args: %@"
//          , (long)callbackId
//          , (synchronous?@"yes":@"no")
//          , arg);
    
    va_list argsList;
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    if(arg != nil){
        [resultArray addObject:arg];
        va_start(argsList, arg);
        while((arg = va_arg(argsList, id)) != nil)
            [resultArray addObject:arg];
        va_end(argsList);
    }
    SBJsonWriter* json = [[SBJsonWriter alloc] init];
    NSString *resultArrayString = [json stringWithObject:resultArray];

    dispatch_async(dispatch_get_main_queue(), ^{
    //dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        //NSLog(@"Pavel - NativeBridge.m, returnResult - before, result: %@", resultArrayString);
        [self performSelectorOnMainThread:@selector(writeJavaScript:) withObject:[NSString stringWithFormat:@"NativeBridge.resultForCallback(%ld,%@);",(long) callbackId, resultArrayString] waitUntilDone:synchronous];
        //NSLog(@"Pavel - NativeBridge.m, returnResult - finish, result: %@", resultArrayString);
        
    });
}

- (void) writeJavaScript:(NSString*) script
{
//    NSLog(@"Pavel - NativeBridge.m, returnResult, writeJavaScript - %@", script);
	[self.webView stringByEvaluatingJavaScriptFromString:script];
}

- (id) invoke:(NSString*)function waitUntilDone:(BOOL)wait args:(id)arg, ...
{
//    NSLog(@"Pavel - NativeBridge.m, invoke: %@, waitUntilDone: %@", function, (wait?@"YES":@"NO"));
    va_list argsList;
    NSMutableArray *arguments = [[NSMutableArray alloc] init];

    if(arg != nil){
        [arguments addObject:arg];
        va_start(argsList, arg);
        while((arg = va_arg(argsList, id)) != nil)
            [arguments addObject:arg];
        va_end(argsList);
    }

    SBJsonWriter* json = [[SBJsonWriter alloc] init];

    NSString* call = [function stringByAppendingString:@"("];
    for (id argument in arguments) {
        NSString* jsonArgument;
        if ([argument isKindOfClass:[NSString class]]) {
            jsonArgument = argument;
        } else {
            jsonArgument = [json stringWithObject:argument];
        }
        call = [call stringByAppendingFormat:@"%@", jsonArgument];
    }
    call = [call stringByAppendingString:@");"];

    if (!wait) {
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:call waitUntilDone:NO];
        
        //[self.webView stringByEvaluatingJavaScriptFromString:call];
        
        return nil;
    } else {
        return [self.webView stringByEvaluatingJavaScriptFromString:call];
    }
}

- (void)webViewDidFinishLoad:(WKWebView *)webView
{
//    NSLog(@"Pavel - NativeBridge.m, webview did finish load!");
}

- (void)webView:(WKWebView *)webView didFailLoadWithError:(NSError *)error
{
//    NSLog(@"Pavel - NativeBridge.m, webview load failed with errorNativeBridge: %@", error);
//    NSLog(@"Pavel - NativeBridge.m, webview load failed with errorNativeBridgedebugDescription: %@", [error debugDescription]);
//    NSLog(@"Pavel - NativeBridge.m, webView didFailLoadWithError - error: %@", [error debugDescription]);
}

- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message {
//    NSLog(@"Pavel - NativeBridge.m, userContentController didReceiveScriptMessage - message: %@", message.name);
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
//    NSLog(@"Pavel - NativeBridge.m webView didFinishNavigation");
  [webView evaluateJavaScript:@"document.title" completionHandler:^(NSString *title, NSError *error) {
   // self.title = title;
  }];
}
// WKWeView calls this method before each request is loaded to confirm whether a request jump is made or not -> js-command:
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
  dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
    NSString *requestString = [[navigationAction.request URL] absoluteString];
    
//    NSLog(@"Pavel - NativeBridge.m, webView decidePolicyForNavigationAction decisionHandler - %@", requestString);
    
    if ([requestString hasPrefix:@"js-command:"]) {

      NSArray *components = [navigationAction.request.URL.absoluteString componentsSeparatedByString:@":"];
      NSString *function = (NSString*)[components objectAtIndex:1];
      int callbackId = [((NSString*)[components objectAtIndex:2]) intValue];
      NSString *argsAsString = [(NSString*)[components objectAtIndex:3]
                                stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
      SBJsonParser* json = [[SBJsonParser alloc] init];
      NSArray *args = (NSArray*)[json objectWithString:argsAsString];

      // not required to call performSelectorOnMainThread but makes it more flexible to change it later for queuing it behind currect execution context.
      [self performSelectorOnMainThread:@selector(handleCall:callbackId:args:) waitUntilDone:YES withObjects:function, @(callbackId), args, nil];
      
//      NSLog(@"Pavel - NativeBridge.m webView decidePolicyForNavigationAction decisionHandler - CANCEL");
        
      decisionHandler(WKNavigationActionPolicyCancel);
    } else if (
        [requestString hasPrefix:@"tel:"]
        || [requestString hasPrefix:@"mailto:"]
    ) {
//        NSLog(@"Pavel - NativeBridge.m, webView decidePolicyForNavigationAction decisionHandler - MAILTO or TEL - %@", requestString);
        
        requestString = [requestString stringByReplacingOccurrencesOfString:@" " withString:@""];
        requestString = [requestString stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:requestString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
//            if (success) {
//                NSLog(@"Pavel - Opened %@", requestString);
//            } else {
//                NSLog(@"Pavel - Failed to open %@", requestString);
//            }
        }];
        decisionHandler(WKNavigationActionPolicyCancel);
  } else {
//      NSLog(@"Pavel - NativeBridge.m webView decidePolicyForNavigationAction decisionHandler - ALLOW");
      decisionHandler(WKNavigationActionPolicyAllow);
  }
  });
}



//#pragma mark - WKUIDelegate
//
//- (void)                        webView:(WKWebView *)webView
//  runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt
//                            defaultText:(NSString *)defaultText
//                       initiatedByFrame:(WKFrameInfo *)frame
//                      completionHandler:(void (^)(NSString * _Nullable))completionHandler
//{
//    NSLog(@"Pavel - prompt, %@", prompt);
//
//    NSData *dataFromString = [prompt dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
//
//    NSDictionary *passedObject = [NSJSONSerialization JSONObjectWithData:dataFromString options:NSJSONReadingMutableContainers error:nil];
//    NSString *type = [passedObject objectForKey:@"type"];
//
//    if ([type isEqualToString:@"SJbridge"]) {
//        self.completionHandler = completionHandler;
//        [NSTimer scheduledTimerWithTimeInterval:5.0
//                                         target:self
//                                       selector:@selector(doSomethingWhenTimeIsUp:)
//                                       userInfo:nil
//                                        repeats:NO];
//    } else {
//        completionHandler(@"Unhandled prompt");
//    }
//
//    NSLog(@"Pavel - prompt, finished");
//}
//
//- (void) doSomethingWhenTimeIsUp:(NSTimer*)t {
//    if (self.completionHandler) {
//        self.completionHandler(@"{cartId: 123, items: 1, name=\"Processed Items\"}");
//    }
//}



@end
