//
//  CFTabItem.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFTabItem.h"
#import "CFPage+JSON.h"
#import "UIColor+Additions.h"
#import "UIImage+Asset.h"

@interface CFTabItem ()

@property (strong, nonatomic) CFPage* cfPage;

@end

@implementation CFTabItem

#pragma mark BSJsonSupported

- (id)proxyForJson
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    [dict setObject:self.title forKey:@"title"];
    if (self.iconName) {
        [dict setObject:self.iconName forKey:@"icon"];
    }
    
    return dict;
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    if ([super init]) {
        if (!receivedObjects) {
            return nil;
        }
        
        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];
        
        self.title = receivedObjects[@"title"];
        self.iconName = receivedObjects[@"icon"];
        self.icon = [UIImage imageFromAsset:self.iconName];
    }
    
    return self;
}


- (id) initWithPage:(CFPage *)page {
    self = [super init];
    if (self) {
        self.cfPage = page;
    }
    
    return self;
}


@end
