//
//  CFTabPage.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFTabPage.h"
#import "CFRuntime.h"
#import "CFTabItem.h"
#import "UIImage+Asset.h"
#import "NSString+NSString_Additions.h"
#import "CFPage+JSON.h"

@interface CFTabPage ()

@property (strong, nonatomic, readwrite) CFPage* page;

@end

@implementation CFTabPage

#pragma mark BSJsonSupported

- (id) proxyForJson
{
    NSMutableDictionary* pageDict = [[self.page proxyForJson] mutableCopy];
    
    NSMutableArray* tabArray = [NSMutableArray arrayWithCapacity:self.tabItems.count];
    for (CFTabItem* tabItem in self.tabItems) {
        [tabArray addObject:[tabItem proxyForJson]];
    }
    
    [pageDict setObject:tabArray forKey:@"tabItems"];
    
    return pageDict;
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    if ([super init]) {
        if (!receivedObjects) {
            return nil;
        }
        
        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];
        
        CFPage* innerPage = [[CFPage alloc] initWithJson:receivedObjects];
        
        NSArray* tabs = receivedObjects[@"tabItems"];
        NSMutableArray* tabItems = [NSMutableArray arrayWithCapacity:tabs.count];
        
        for (NSDictionary* itemDict in tabs) {
            CFTabItem* tabItem = [[CFTabItem alloc] initWithJson:itemDict];
            [tabItems addObject:tabItem];
        }
        
        self.page = innerPage;
        self.tabItems = tabItems;
    }
    
    return self;
}


- (id) initWithPage:(CFPage *)page {
    self = [super init];
    if (self) {
        self.page = page;
        
        self.tabItems = [self readTabItems];
        self.selectedTabBarIndex = [self readSelectedTabBarIndex];
    }
    
    return self;
}

- (NSInteger) readSelectedTabBarIndex
{
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
    
    NSError* error;
    NSArray* elements = [pageElement nodesForXPath:@"./tabview/@selectedtab" error:&error];
    
    NSInteger index = 0;
    
    if (elements && elements.count > 0) {
        index = [[elements[0] stringValue] integerValue];
    }
    
    return index;
}

- (NSArray*) readTabItems {
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
    
    NSError* error;
    NSArray* tabElements = [pageElement nodesForXPath:@"./tabview/tabitem" error:&error];
    NSMutableArray* tabItems = [NSMutableArray arrayWithCapacity:tabElements.count];
    
    for (id tabElement in tabElements) {
        CFPage* tabPage = [[CFRuntime instance] getPageWithId:[[tabElement attributeForName:@"content"] stringValue]];
        NSString* title = [[tabElement attributeForName:@"title"] stringValue];
        
        if ([NSString isEmpty:title]) {
            title = tabPage.title;
        }
        
        NSString* iconString = [[tabElement attributeForName:@"icon"] stringValue];
        
        CFTabItem* tabItem = [[CFTabItem alloc] initWithPage:tabPage];
        tabItem.title =title;
        if (![NSString isEmpty:iconString]) {
            tabItem.iconName = iconString;
            tabItem.icon = [UIImage imageFromAsset:iconString];
        }
        
        [tabItems addObject:tabItem];
    }
    
    return tabItems;
}

@end
