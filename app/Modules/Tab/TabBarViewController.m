//
//  TabBarViewController.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "TabBarViewController.h"
#import "DefaultNavigationImpl.h"
#import "NavigationViewController.h"
#import "CFTabPage.h"
#import "CFTabItem.h"
#import "CFRuntime.h"
#import "UIViewController+Container.h"

@interface TabBarViewController () <NavigationService, NavigationResultDelegate>
{
    BOOL _initialized;
}

@end

@implementation TabBarViewController

@synthesize navigationContext = _navigationContext;
@synthesize page = _page;
@synthesize delegate = _delegate;
@synthesize typedPage = _typedPage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self->_initialized = NO;
    }
    return self;
}

- (void) setNavigationContext:(id)navigationContext {
    for (id container in self.viewControllers) {
        [[container viewControllers][0] setNavigationContext:self.navigationContext];
    }
}

- (void) setPage:(CFPage *)page
{
    BOOL reloadSubPages = !_page;
    
    if (_page != page) {
        _page = page;
        if (_page && reloadSubPages) {
            // initial loading of page
            [self updateAppearance];
        }
    }
}

- (void) updateAppearance
{
    CFTabPage* tabPage = (CFTabPage*) self.typedPage;
    if (!tabPage) {
        tabPage = [[CFTabPage alloc] initWithPage:_page];
        self.typedPage = tabPage;
    }
    
    if (!self->_initialized) {
        NSArray* tabItems = tabPage.tabItems;
        
        NSMutableArray* viewControllers = [NSMutableArray arrayWithCapacity:tabItems.count];
        
        for (id tabItem in tabItems) {
            
            UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTitle:[tabItem title] image:[((CFTabItem*) tabItem) icon] tag:0];
            UIViewController<Navigation>* viewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:[tabItem cfPage]];
            viewController.navigationService.page = [tabItem cfPage];
            viewController.navigationService.navigationContext = self.navigationContext;
            viewController.navigationService.delegate = self;
            viewController.hidesBottomBarWhenPushed = NO;
            
            BaseNavigationController* container = [[BaseNavigationController alloc] initWithRootViewController:viewController];
            
            container.tabBarItem = tabBarItem;
            [viewControllers addObject:container];
        }
        self.title = self.page.title;
        
        [self setViewControllers:viewControllers];
        self.selectedIndex = tabPage.selectedTabBarIndex;
        
        self->_initialized = YES;
    } else {
        NSArray* viewControllers = [self viewControllers];
        for (int i = 0; i < viewControllers.count; i++) {
            if (tabPage.tabItems.count > i) {
                CFTabItem* tabItem = tabPage.tabItems[i];
                UIViewController* viewController = viewControllers[i];
                UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTitle:[tabItem title] image:[tabItem icon] tag:0];
                viewController.tabBarItem = tabBarItem;
            }
        }
        self.title = self.page.title;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) isContainer
{
    return YES;
}

#pragma mark Navigation

- (id<NavigationService>) navigationService {
    return self;
}

- (void) navigateToPage:(CFPage *)page withContext:(id)navigationContext {
    //invalid, navigation is only allowed from one of the root pages.
}
- (void) navigateToPageAndAddToStack:(CFPage *)page withContext:(id)navigationContext {
    //invalid, navigation is only allowed from one of the root pages.
}

- (void) navigateBackWithResult:(id)result {
    // invalid, navigation is only allowed fromone of the root pages.
}
- (void) navigateBackWithResult:(id)result onStackView:(NSString *) pageId {
    // invalid, navigation is only allowed fromone of the root pages.
}
- (void) navigateBackWithResultToRoot:(id)result onStackView:(NSString *) pageId{
    
}
- (void) navigateToRoot {
    // invalid, navigation is only allowed fromone of the root pages.
}


- (void) dismissNavigation:(id<Navigation>)navigation withResult:(id)result {
    // dismiss self
    if (self.delegate) {
        [self.delegate dismissNavigation:self withResult:result];
    }
}
- (void) dismissNavigationOnStack:(id<Navigation>)navigation withResult:(id)result andPageId: (NSString *) pageId {
    // dismiss self
    if (self.delegate) {
        [self.delegate dismissNavigationOnStack:self withResult:result andPageId:pageId];
    }
}
- (void) dismissNavigationToRoot:(id<Navigation>)navigation withResult:(id)result andPageId: (NSString *) pageId {
    // dismiss self
    if (self.delegate) {
        [self.delegate dismissNavigationOnStack:self withResult:result andPageId:pageId];
    }
}

- (void) applyHeaderAttributesOnNavigationController:(UINavigationController *)navigationController andButtonTarget:(id<ButtonTarget>)target
{
    [DefaultNavigationImpl clearPageHeaderAttributes:navigationController];
    
    BOOL shouldApplyOwnHeader = YES;
    
    // Do not apply own header if a subview has hidden the tab bar for navigating to one of their "children".
    if ([self.selectedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* container = (UINavigationController*) self.selectedViewController;
        
        shouldApplyOwnHeader = container.viewControllers.count <= 1;
    }
    
    if (shouldApplyOwnHeader) {
        [DefaultNavigationImpl applyPageHeaderAttributes:self.navigationService.page.pageHeader toNavigationController:navigationController andButtonTarget:target];
    }
}

@end
