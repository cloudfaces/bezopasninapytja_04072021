//
//  SplashViewController.m
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "SplashViewController.h"
#import "CFDynamicFileUpdate.h"

@interface SplashViewController () <AppDownloadViewControllerDelegate>
@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([BundleSettings isRemoteApp]) {
        [self showAppDownloadViewController];
    } else {
        [self initRuntimeFromUrl:nil];
    }
}

- (BOOL) isContainer
{
    return YES;
}

@end
