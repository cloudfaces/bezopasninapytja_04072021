//
//  CFSlidingTabPage.h
//  app
//
//  Created by LeyLa on 4/30/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "BSJsonSupported.h"
#import "TypedPage.h"

@interface CFSlidingTabPage : NSObject <BSJsonSupported, TypedPage>

@property (nonatomic) NSArray* tabItems;
@property (nonatomic) NSInteger selectedTabBarIndex;
@property (strong, nonatomic, readonly) CFPage* page;
- (NSArray*) readTabItems;
- (id) initWithPage:(CFPage*) page;

@property (nonatomic) NSString* navbarBackgroudColor;
@property (nonatomic) NSString* iconColor;
@property (nonatomic) NSString* iconColorSelected;

@end
