//
//  SlidingTabsHelper.h
//  app
//
//  Created by LeyLa on 5/25/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SlidingTabsHelper : NSObject

@property (strong, nonatomic) UINavigationController *nav;
@property (strong, nonatomic) UIWindow *window;
@property BOOL isEnable;
@property BOOL slidingTabs;
@property (strong, nonatomic) CFPage *page;
@property (strong, nonatomic) NSString *pageId;
@property (strong, nonatomic) NSString *checkIsEnable;
@property (strong, nonatomic) NSString *navContext;


+ (SlidingTabsHelper *)sharedInstance;
- (instancetype) init;

-(void) setSlidingTabsEnable: (NSString *) string;
-(NSString *) checkIsEnable;

@end


