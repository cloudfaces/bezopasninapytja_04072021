//
//  Storage.h
//  app
//
//  Created by Bernhard Kunnert on 11.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "DataPool.h"

@interface Storage : DataPool

- (void) commitChanges;
- (id) removeValue;

+ (Storage*) instance;

@end
