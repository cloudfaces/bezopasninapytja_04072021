//
//  DataPool.h
//  app
//
//  Created by Bernhard Kunnert on 11.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataPool : NSObject
{
@protected
    NSMutableDictionary* _dictionary;
}

+ (DataPool*) instance;

- (void) writeValue:(id) value forKey:(NSString*) key;
- (id) readValueForKey:(NSString*) key;
- (BOOL) valueExistsForKey:(NSString*) key;
- (id) removeValue;
- (id) readValue;
@end
