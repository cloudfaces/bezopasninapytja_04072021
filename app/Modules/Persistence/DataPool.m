//
//  DataPool.m
//  app
//
//  Created by Bernhard Kunnert on 11.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "DataPool.h"

@implementation DataPool

- (id) init {
    self = [super init];
    if (self) {
        _dictionary = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

+ (DataPool*) instance {
    static DataPool* _instance = nil;
    if (!_instance) {
        _instance = [[DataPool alloc] init];
    }
    
    return _instance;
}

- (void) writeValue:(id) value forKey:(NSString*) key {
    if (!value || value == [NSNull null]) {
        [_dictionary removeObjectForKey:key];
    } else {
        [_dictionary setObject:value forKey:key];
    }
}

- (id) readValueForKey:(NSString*) key {
    return [_dictionary objectForKey:key];
}

- (BOOL) valueExistsForKey:(NSString*) key {
    return [self readValueForKey:key] != nil;
}
- (id) readValue {
    return _dictionary;
}

- (id) removeValue {
   // [self readValue];
    [_dictionary removeAllObjects];
    _dictionary = [[NSMutableDictionary alloc] init];

    return _dictionary;
}


@end
