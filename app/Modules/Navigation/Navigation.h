//
//  Navigation.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "TypedPage.h"

@protocol NavigationService;

@protocol ButtonTarget <NSObject>

@optional
@property(nonatomic, strong) CFHeaderButton* rightButton;
@property(nonatomic, strong) CFHeaderButton* leftButton;
@property(nonatomic, strong) CFContentView* contentView;

- (void) handleRightButtonPress:(id) sender;
- (void) handleLeftButtonPress:(id) sender;
- (void) handleContentViewPress:(id) sender;

@end

@protocol Navigation <NSObject>

@property (readonly, nonatomic) id<NavigationService> navigationService;
@property (strong, nonatomic) id<TypedPage> typedPage;

@required
- (void) applyHeaderAttributesOnNavigationController:(UINavigationController*) navigationController andButtonTarget:(id<ButtonTarget>) target;
- (void) updateAppearance;

@end

@protocol NavigationResultDelegate <NSObject>

- (void) dismissNavigation:(id<Navigation>) navigation withResult:(id) result;
- (void) dismissNavigationOnStack:(id<Navigation>) navigation withResult:(id) result andPageId:(NSString *)pageId;
- (void) dismissNavigationToRoot:(id<Navigation>) navigation withResult:(id) result andPageId:(NSString *)pageId;

@end

@protocol NavigationService <NSObject>

@property (strong, nonatomic) id navigationContext;
@property (strong, nonatomic) CFPage* page;
@property (weak, nonatomic) id<NavigationResultDelegate> delegate;

- (void) navigateToPage:(CFPage *)page withContext:(id) navigationContext;
- (void) navigateToPageAndAddToStack:(CFPage *)page withContext:(id) navigationContext;

- (void) navigateBackWithResult:(id) result;
- (void) navigateBackWithResult:(id) result onStackView:(NSString *) pageId;
- (void) navigateBackWithResultToRoot:(id)result onStackView:(NSString *) pageId;

@optional
- (void) navigateToMenuPage:(CFPage *)page withContext:(id) navigationContext;

@end
