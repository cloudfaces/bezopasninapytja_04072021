//
//  NavigationViewController.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Navigation.h"
#import "DefaultNavigationImpl.h"

@protocol NavigationViewControllerOverrides

@optional
- (void) onResultReceived:(id) result;

@end

@interface NavigationViewController : BaseViewController <Navigation, NavigationViewControllerOverrides>

@property (readonly, nonatomic) DefaultNavigationImpl* navigationImpl;

/**
 * Tries to find menu navigation handler in view hierarchy to call or invokes default navigation
 */
- (void) tryMenuNavigation:(CFPage *)page withContext:(id)navigationContext;
- (void) tryMenuNavigationFromStack:(CFPage *)page withContext:(id)navigationContext;
- (id<TypedPage>) findPageInCurrentStack:(NSString*) pageName;
- (void) updatePage:(NSString*) pageName inCurrentStack:(id<TypedPage>) typedPage;
- (void) refreshStack;

@end
