//
//  DefaultNavigationImpl.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "DefaultNavigationImpl.h"
#import "CFRuntime.h"
#import "MainMenu.h"
#import "UIImage+Asset.h"
#import "UIImage+Average.h"
#import "NativeBridge.h"
#import "MenuPositionHelper.h"
#import "AppDelegate.h"
#import "SlidingTabsHelper.h"

@interface DefaultNavigationImpl () <NavigationResultDelegate>

@property (weak, nonatomic) UIViewController<Navigation>* owner;
@property (weak, nonatomic) UIViewController<Navigation>* navigationTarget;

@end

@implementation DefaultNavigationImpl


@synthesize navigationContext = _navigationContext;
@synthesize page = _page;
@synthesize delegate = _delegate;

+ (void) applyPageHeaderAttributes:(CFPageHeader*) header toNavigationController:(UINavigationController*) navigationController andButtonTarget:(id<ButtonTarget>) target
{
    if (header) {
        // Apply header attributes:
        // - background image
        // - background color
        // - title color
        NSMutableDictionary *titleAttributes = [[navigationController.navigationBar titleTextAttributes] mutableCopy];
        if (!titleAttributes) {
            titleAttributes = [NSMutableDictionary dictionary];
        }
        if (header.titleColor) {
            [titleAttributes setObject:header.titleColor forKey:NSForegroundColorAttributeName];
        }
        
        if (header.backgroundImage) {
            [navigationController.navigationBar setBackgroundImage:header.backgroundImage forBarMetrics:UIBarMetricsDefault];
        } else if (header.backgroundColor) {
            [navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            if ([PlatformUtil isIos7]) {
                navigationController.navigationBar.barTintColor = header.backgroundColor;
            } else {
                navigationController.navigationBar.tintColor = header.backgroundColor;
            }
        }
        
        // apply navigation item attribtues:
        // - titleView (for logo)
        UINavigationItem* navigationItem = navigationController.visibleViewController.navigationItem;
        if (header.titleImage) {
            UIImageView* titleView = [[UIImageView alloc] initWithImage:header.titleImage];
            
            titleView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            titleView.contentMode=UIViewContentModeScaleAspectFit;
            
            CGRect navbarFrame = navigationController.navigationBar.frame;
            navbarFrame.size.width = 0;
            
            titleView.frame = navbarFrame;
            navigationItem.titleView = titleView;
        } else {
            navigationItem.titleView = nil;
        }
        
        // apply right button
        if (header.rightButtonsArray) {
            NSMutableArray *rightButtonItems = [[NSMutableArray alloc] init];
            [CFRuntime instance].rightButtons = header.rightButtonsArray;
            for (CFHeaderButton *rightButtonItem in header.rightButtonsArray) {
                UIBarButtonItem* rightButton;
                
                SEL rightButtonSelector = @selector(handleRightButtonPress:);
                id buttonTarget = target;
                if (![target respondsToSelector:rightButtonSelector]) {
                    rightButtonSelector = nil;
                    buttonTarget = nil;
                } else {
                    [target setRightButton:rightButtonItem];
                }
                if (rightButtonItem.icon) {
                    rightButton = [[UIBarButtonItem alloc] initWithImage:rightButtonItem.icon style:UIBarButtonItemStylePlain target:buttonTarget action:rightButtonSelector];
                } else {
                    rightButton = [[UIBarButtonItem alloc] initWithTitle:rightButtonItem.text style:UIBarButtonItemStylePlain target:buttonTarget action:rightButtonSelector];
                }
                //                if (rightButtonItem.iconName) {
                //
                //                 //   rightButtonItem.icon = [UIImage imageNamed:rightButtonItem.iconName];
                //                    rightButtonItem.icon = [UIImage imageFromAsset:rightButtonItem.iconName];
                //                    rightButton = [[UIBarButtonItem alloc] initWithImage:rightButtonItem.icon style:UIBarButtonItemStylePlain target:buttonTarget action:rightButtonSelector];
                //                } else {
                //                    rightButton = [[UIBarButtonItem alloc] initWithTitle:rightButtonItem.text style:UIBarButtonItemStylePlain target:buttonTarget action:rightButtonSelector];
                //                }
                if (rightButtonItem.textColor)
                {
                    if ([PlatformUtil isIos7])
                    {
                        rightButton.tintColor = rightButtonItem.textColor;
                    } else {
                        [rightButton setTitleTextAttributes:
                         @{
                           NSForegroundColorAttributeName : rightButtonItem.textColor,
                           NSShadowAttributeName : [UIColor clearColor]
                           } forState:UIControlStateNormal];
                    }
                } else {
                    if ([PlatformUtil isIos7])
                    {
                        rightButton.tintColor = navigationController.view.window.tintColor;
                    }
                }
                
                if (header.backgroundImage && ![PlatformUtil isIos7]) {
                    // set the tint color of the background to a mered color of the background image of the bar
                    rightButton.tintColor = header.backgroundImage.mergedColor;
                    
                    // alternatively hide the background button image at all (like iOS 7)
                    //[rightButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
                }
                
                [rightButtonItems addObject:rightButton];
                navigationItem.rightBarButtonItems = rightButtonItems;
            }
        }
        
        // apply back button
        // apply left button
        if (header.leftButton) {
            UIBarButtonItem* leftButton;
            
            SEL leftButtonSelector = @selector(handleLeftButtonPress:);
            id buttonTarget = target;
            if (![target respondsToSelector:leftButtonSelector]) {
                leftButtonSelector = nil;
                buttonTarget = nil;
            } else {
                [target setLeftButton:header.leftButton];
            }
            
            if (header.leftButton.icon) {
                //   leftButton = [[UIBarButtonItem alloc] initWithImage:header.leftButton.icon style:UIBarButtonItemStylePlain target:nil action:nil];
                //SEL buttonClick = @selector(buttonClick:);
                
                
                navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:header.leftButton.iconName] style:UIBarButtonItemStylePlain target:buttonTarget action:leftButtonSelector];
                
                navigationItem.leftBarButtonItem.tintColor = header.leftButton.textColor;
                
                //  [navigationItem setHidesBackButton:YES animated:NO];
                // [navigationItem setBackBarButtonItem:nil];
                //  navigationItem.hidesBackButton = YES;
                // navigationItem.backBarButtonItem =  navigationItem.leftBarButtonItem;
            } else if (![NSString isEmpty:header.leftButton.text]){
                leftButton = [[UIBarButtonItem alloc] initWithTitle:header.leftButton.text style:UIBarButtonItemStylePlain target:buttonTarget action:leftButtonSelector];
            }
            else {
                NSString *leftButtonText = @" ";
                leftButton = [[UIBarButtonItem alloc] initWithTitle:leftButtonText style:UIBarButtonItemStylePlain target:buttonTarget action:leftButtonSelector];
            }
            if (header.leftButton.textColor)
            {
                if ([PlatformUtil isIos7])
                {
                    leftButton.tintColor = header.leftButton.textColor;
                    navigationController.navigationBar.tintColor = header.leftButton.textColor;
                } else {
                    [leftButton setTitleTextAttributes:
                     @{
                       NSForegroundColorAttributeName : header.leftButton.textColor,
                       NSShadowAttributeName : [UIColor clearColor]
                       } forState:UIControlStateNormal];
                }
            }
            
            if (header.backgroundImage && ![PlatformUtil isIos7]) {
                // set the tint color of the background to a mered color of the background image of the bar
                leftButton.tintColor = header.backgroundImage.mergedColor;
                
                // alternatively hide the background button image at all (like iOS 7)
                //[rightButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            }
            NSArray* viewControllers = navigationController.viewControllers;
            NSInteger viewControllerIndex = [navigationController.viewControllers indexOfObject:navigationController.visibleViewController];
            
            if (viewControllerIndex > 0 && viewControllerIndex != NSNotFound) {
                navigationItem = [viewControllers[viewControllerIndex - 1] navigationItem];
                navigationItem.backBarButtonItem = leftButton;
            }
        }
        
        // apply content view button
        if (header.contentView) {
            // Clear Title Text
            [navigationController.navigationBar setTitleTextAttributes:nil];
            UINavigationItem* navigationItem = navigationController.visibleViewController.navigationItem;
            navigationItem.title = nil;
            
            //Set Content View
            NSString *contentViewType = header.contentView.type;
            UIColor *contentViewColor = header.contentView.color;
            NSString *contentViewText = header.contentView.text;
            
            if ([contentViewType isEqual:@"text"]) {
                contentViewText = @"";
                
                UIImage *image;
                UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setSelected:NO];
                [button setTitle:contentViewText forState:UIControlStateNormal];
                
                CGSize size = [header.contentView.text sizeWithAttributes: @{NSFontAttributeName:[UIFont systemFontOfSize:18.0f]}];
                button.frame = CGRectMake(0, 0, size.width,navigationController.navigationBar.frame.size.height);
                button.titleLabel.adjustsFontSizeToFitWidth = YES;
                button.titleLabel.minimumScaleFactor = 0.5;
                //                button.transform = CGAffineTransformMakeScale(-1.0, 1.0);
                //                button.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
                //                button.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
                if (contentViewColor) {
                    [button setTitleColor:contentViewColor forState:UIControlStateNormal];
                    button.tintColor = contentViewColor;
                }
                
                [button setImage:image forState:UIControlStateNormal];
                [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
                navigationItem.titleView = button;
            }
            
            if ([contentViewType isEqual:@"toggle"]) {
                //content view text
                NSDate *todayDate = [NSDate date]; // get today date
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
                [dateFormatter setDateFormat:@"dd MMM yyyy"]; //Here we can set the format which we need
                NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];// here convert date in
                
                contentViewText = convertedDateString;
                
                UIImage *image;
                
                UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setSelected:NO];
                
                SEL contentViewSelector = @selector(handleContentViewPress:);
                id buttonTarget = target;
                if (![target respondsToSelector:contentViewSelector]) {
                    contentViewSelector = nil;
                    buttonTarget = nil;
                } else {
                    [target setContentView:header.contentView];
                }
                
                [button addTarget:buttonTarget action:contentViewSelector forControlEvents:UIControlEventTouchUpInside];
                
                [button setSelected:NO];
                if(button.isSelected)
                {
                    image = [[UIImage imageNamed:@"arrow-up.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [button setImage:image forState:UIControlStateNormal];
                } else {
                    image = [[UIImage imageNamed:@"arrow-down.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [button setImage:image forState:UIControlStateNormal];
                }
                
                CGSize size = [contentViewText sizeWithAttributes:
                               @{NSFontAttributeName:
                                     [UIFont systemFontOfSize:18.0f]}];
                
                [button setTitle:contentViewText forState:UIControlStateNormal];
                button.frame = CGRectMake(0, 0, size.width,navigationController.navigationBar.frame.size.height);
                button.transform = CGAffineTransformMakeScale(-1.0, 1.0);
                button.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
                button.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
                if (contentViewColor) {
                    [button setTitleColor:contentViewColor forState:UIControlStateNormal];
                    
                    button.tintColor = contentViewColor;
                }
                
                [button setImage:image forState:UIControlStateNormal];
                [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
                navigationItem.titleView = button;
            }
        }
        [navigationController.navigationBar setTitleTextAttributes:titleAttributes];
    }
}

+ (void) changeTitle:(UINavigationController*) navigationController newTitle: (NSString *) newTitle {
    UINavigationItem* navigationItem = navigationController.visibleViewController.navigationItem;
    CGSize size = [newTitle sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:18.0f]}];
    
    UIButton *button = (UIButton *) navigationItem.titleView;
    button.frame = CGRectMake(0, 0, size.width,navigationController.navigationBar.frame.size.height);
    
    [button setTitle:newTitle forState:UIControlStateNormal];
}

+ (void) clearView:(UINavigationController*) navigationController {
    UINavigationItem* navigationItem = navigationController.visibleViewController.navigationItem;
    navigationItem.titleView = nil;
}

+ (void) clearPageHeaderAttributes:(UINavigationController*) navigationController
{
    if ([PlatformUtil isIos7]) {
        navigationController.navigationBar.barTintColor = nil;
        navigationController.navigationBar.tintColor = nil;
    }
    navigationController.navigationBar.tintColor = nil;
    
    [navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [navigationController.navigationBar setTitleTextAttributes:nil];
    
    UINavigationItem* navigationItem = navigationController.visibleViewController.navigationItem;
    navigationItem.rightBarButtonItem = nil;
    navigationItem.backBarButtonItem = nil;
}

- (void) delegateParentPageHeaderAttributes:(CFPageHeader*) header toNavigationController:(UINavigationController*) navigationController andButtonTarget:(id<ButtonTarget>) target
{
    UIViewController* parent = self.owner.parentViewController;
    id<Navigation> conformingParent = [[parent class] conformsToProtocol:@protocol(Navigation)] ? (id<Navigation>) parent : nil;
    
    while (!conformingParent && parent) {
        parent = parent.parentViewController;
        if (parent) {
            conformingParent = [[parent class] conformsToProtocol:@protocol(Navigation)] ? (id<Navigation>) parent : nil;
        }
    }
    
    if (conformingParent) {
        [conformingParent applyHeaderAttributesOnNavigationController:navigationController andButtonTarget:target];
    } else {
        [DefaultNavigationImpl clearPageHeaderAttributes:navigationController];
    }
}

+ (void) navigateBackToViewController:(UIViewController *)viewController {
    [viewController.navigationController popToViewController:viewController animated:YES];
}

- (UIViewController<Navigation>*) createAndParametrizeViewController:(CFPage*) page withContext:(id)navigationContext {
    UIViewController<Navigation>* viewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:page];
    
    viewController.navigationService.navigationContext = navigationContext;
    viewController.navigationService.delegate = self;
    viewController.navigationService.page = page;
    
    [[NavigationWebViewStack instance] setWebView:viewController andPageId:page.uniqueId];
    
    return viewController;
}

- (UIViewController<Navigation>*) createAndParametrizeViewControllerAndAddToStack:(CFPage*) page withContext:(id)navigationContext {
    UIViewController<Navigation>* viewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:page];
    
    viewController.navigationService.navigationContext = navigationContext;
    viewController.navigationService.delegate = self;
    viewController.navigationService.page = page;
    
    [[NavigationWebViewStack instance] setWebView:viewController andPageId:page.uniqueId];
    
    return viewController;
}

- (void) navigateToPage:(CFPage *)page withContext:(id)navigationContext {
    //SlidingTabs
    UIViewController<Navigation>* viewController = [self createAndParametrizeViewControllerAndAddToStack:page withContext:navigationContext];
    self.navigationTarget = viewController;
    [self.owner.navigationController pushViewController:viewController animated:YES];
}

- (void) navigateToPageAndAddToStack:(CFPage *)page withContext:(id)navigationContext {
    //SlidingTabs
    UIViewController<Navigation>* viewController = [self createAndParametrizeViewControllerAndAddToStack:page withContext:navigationContext];
    self.navigationTarget = viewController;
    [self.owner.navigationController pushViewController:viewController animated:YES];
}

- (void) navigateBackWithResult:(id)result {
    if (self.delegate) {
        [self.delegate dismissNavigation:self.owner withResult:result];
    }
}

- (void) navigateBackWithResult:(id)result onStackView:(NSString *) pageId {
    if (self.delegate) {
        [self.delegate dismissNavigationOnStack: self.owner withResult:result andPageId:pageId];
    }
}

- (void) navigateBackWithResultToRoot:(id)result onStackView:(NSString *) pageId {
    if (self.delegate) {
        [self.delegate dismissNavigationToRoot: self.owner withResult:result andPageId:pageId];
    }
}

- (void) dismissNavigationOnStack:(id<Navigation>)navigation withResult:(id)result andPageId:(NSString *) pageId {
    if (self.navigationTarget == navigation) {
        self.navigationTarget = nil;
        
        NSString *pageIdDict = [[NSString alloc] init];
        NSArray *webViewArray = [NavigationWebViewStack instance].webViews;
        UIViewController <Navigation> * popViewController;
        for (NSDictionary *dict in webViewArray) {
            pageIdDict = dict[@"pageId"];
            if ([pageIdDict isEqualToString:pageId]) {
                popViewController = dict[@"webView"];
            }
        }
        popViewController.navigationService.navigationContext = result;
        [self.owner.navigationController popToViewController:popViewController animated:YES];
        
        if (self.resultDelegate && result) {
            [self.resultDelegate navigationCompleted:self withResult:result];
        }
    }
}

- (void) dismissNavigationToRoot:(id<Navigation>)navigation withResult:(id)result andPageId:(NSString *) pageId {
    if (self.navigationTarget == navigation) {
        self.navigationTarget = nil;
        UIViewController <Navigation> * popViewController;
        if ([pageId isEqualToString:@""]) {
            popViewController = [self.owner.navigationController.viewControllers objectAtIndex:0];
            popViewController.navigationService.navigationContext = result;
            [self.owner.navigationController popToViewController:popViewController animated:YES];
            if (self.resultDelegate && result) {
                [self.resultDelegate navigationCompleted:self withResult:result];
            }
        }
    }
}

- (void) dismissNavigation:(id<Navigation>)navigation withResult:(id)result {
    if (self.navigationTarget == navigation) {
        self.navigationTarget = nil;
        [self.owner.navigationController popToViewController:self.owner animated:YES];
        if (self.resultDelegate && result) {
            [self.resultDelegate navigationCompleted:self withResult:result];
        }
    }
}

- (id) init {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"-init is not a valid initializer for the class"
                                 userInfo:nil];
    return nil;
}

- (id) initWithOwner:(UIViewController<Navigation> *)owner {
    self = [super init];
    if (self) {
        self.owner = owner;
    }
    return  self;
}

+ (void) updateBadgeNew:(UINavigationController*) navigationController {
    SlidingViewController *slidingView = [[SlidingViewController alloc] init];
    UINavigationItem* navigationItem = navigationController.visibleViewController.navigationItem;
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    if ([[MenuPositionHelper sharedInstance].menuPosition isEqual:@"right"]) {
        [customButton addTarget:slidingView action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        [customButton addTarget:slidingView action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // Customize your button as you want, with an image if you have a pictogram to display for example
    [customButton setImage:[UIImage imageNamed:@"reveal-icon"] forState:UIControlStateNormal];
    
    // Then create and add our custom BadgeBarButtonItem
    BadgeBarButtonItem *barButton = [[BadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    
    NSString *badge = [Badges sharedInstance].getBadgeNumber;
    
    // Set a value for the badge
    barButton.badgeValue = badge;
    barButton.badgeOriginX = 13;
    barButton.badgeOriginY = -9;
    
    navigationItem.leftBarButtonItem = barButton;
}

+ (void) setMenuPosition:(NSString *)menuPosition {
    
}

@end
