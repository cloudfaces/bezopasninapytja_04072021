//
//  NavigationViewController.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "NavigationViewController.h"
#import "CFRuntime.h"
#import "DefaultNavigationImpl.h"
#import "Navigation.h"
#import "UIColor+Additions.h"
#import "SlidingTabsHelper.h"
#import "AppDownloadViewController.h"

@interface NavigationViewController () <DefaultNavigationImplResultDelegate, ButtonTarget>

@property (strong, nonatomic) DefaultNavigationImpl* navigationImpl;
@property (weak, nonatomic) NSString* menuPosition;

@end

@implementation NavigationViewController

@synthesize typedPage = _typedPage;

- (id<NavigationService>) navigationService {
    return self.navigationImpl;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationImpl = [[DefaultNavigationImpl alloc] initWithOwner:self];
        self.navigationImpl.resultDelegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = self.navigationService.page.title;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNavigationControllerAttributes];
}

- (void) setNavigationControllerAttributes
{
    self.navigationItem.title = self.navigationService.page.title;
    [self applyHeaderAttributesOnNavigationController:self.navigationController andButtonTarget:self];
    
    [self.parentViewController setNeedsStatusBarAppearanceUpdate];
}

- (void) updateAppearance
{
    [self setNavigationControllerAttributes];
}

- (void) refreshStack
{
    NSMutableArray* stack = [NSMutableArray array];
    id<Navigation> start = self;
    
    while (start) {
        [stack insertObject:start atIndex:0];
        
        start = (id<Navigation>) [NavigationViewController findViewController:((UIViewController*) start).parentViewController conformingToProtocol:@protocol(Navigation) andPredicate:nil];
    }
    
    [stack enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj updateAppearance];
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return [self.navigationService.page.pageHeader.statusBarColor isEqualToColor:[UIColor blackColor]] ? UIStatusBarStyleDefault : UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tryMenuNavigation:(CFPage *)page withContext:(id)navigationContext
{
    
    // Find menu container
    // Go down to parent view controllers until a controller is found who implements Navigation
    UIViewController* parent = self.parentViewController;
    id<Navigation> conformingParent = [[parent class] conformsToProtocol:@protocol(Navigation)] && [[(id) parent navigationService] respondsToSelector:@selector(navigateToMenuPage:withContext:)] ? (id<Navigation>) parent : nil;
    
    while (!conformingParent && parent) {
        parent = parent.parentViewController;
        if (parent) {
            conformingParent = [[parent class] conformsToProtocol:@protocol(Navigation)] && [[(id) parent navigationService] respondsToSelector:@selector(navigateToMenuPage:withContext:)] ? (id<Navigation>) parent : nil;
        }
    }
    
    if (conformingParent && [self.navigationController.viewControllers indexOfObject:self] == 0) {
        // use menu navigation
        [[conformingParent navigationService] navigateToMenuPage:page withContext:navigationContext];
    } else {
        // no parent support it or wrong context -> use default navigaton
        [self.navigationService navigateToPage:page withContext:navigationContext];
    }
}

- (void) tryMenuNavigationFromStack:(CFPage *)page withContext:(id)navigationContext
{
    //SlidingTabs
    if ([[SlidingTabsHelper sharedInstance].checkIsEnable isEqual:@"YES"]) {
        [[AppDownloadViewController alloc] initRuntimeFromUrl:nil];
        [[SlidingTabsHelper sharedInstance] setSlidingTabsEnable:@"NO"];
    }else{
        // Find menu container
        // Go down to parent view controllers until a controller is found who implements Navigation
        UIViewController* parent = self.parentViewController;
        id<Navigation> conformingParent = [[parent class] conformsToProtocol:@protocol(Navigation)] && [[(id) parent navigationService] respondsToSelector:@selector(navigateToMenuPage:withContext:)] ? (id<Navigation>) parent : nil;

        while (!conformingParent && parent) {
            parent = parent.parentViewController;
            if (parent) {
                conformingParent = [[parent class] conformsToProtocol:@protocol(Navigation)] && [[(id) parent navigationService] respondsToSelector:@selector(navigateToMenuPage:withContext:)] ? (id<Navigation>) parent : nil;
            }
        }
        
        // TODO - WTH is this...?
        if (conformingParent && [self.navigationController.viewControllers indexOfObject:self] == 0) {
            // use menu navigation
            [[conformingParent navigationService] navigateToMenuPage:page withContext:navigationContext];
        } else {
            [[conformingParent navigationService] navigateToMenuPage:page withContext:navigationContext];
        }
    }
}

+ (UIViewController*) findViewController:(UIViewController*) start conformingToProtocol:(Protocol*)protocol andPredicate:(BOOL (^)(UIViewController*)) predicateBlock
{
    id conforming = [[start class] conformsToProtocol:protocol] && (!predicateBlock || predicateBlock(start)) ? (id) start : nil;
    
    if (!conforming && start) {
        start = start.parentViewController;
        if (start) {
            conforming = [NavigationViewController findViewController:start conformingToProtocol:protocol andPredicate:predicateBlock];
        }
    }
    
    return conforming;
}

- (id<TypedPage>) findPageInCurrentStack:(NSString*) pageName
{
    id<Navigation> result = (id<Navigation>) [NavigationViewController findViewController:self conformingToProtocol:@protocol(Navigation) andPredicate:^BOOL(UIViewController * navigationViewController) {
        
        id<Navigation> navigation = (id<Navigation>) navigationViewController;
        return [navigation.navigationService.page.uniqueId isEqualToString:pageName];
    }];
    
    if (result) {
        return result.typedPage;
    } else {
        return nil;
    }
}

- (void) updatePage:(NSString*) pageName inCurrentStack:(id<TypedPage>) typedPage
{
    UIViewController* result = [NavigationViewController findViewController:self conformingToProtocol:@protocol(Navigation) andPredicate:^BOOL(UIViewController * navigationViewController) {
        
        id<Navigation> navigation = (id<Navigation>) navigationViewController;
        return [navigation.navigationService.page.uniqueId isEqualToString:pageName];
    }];
    
    id<Navigation> navigation = (id<Navigation>) result;
    if (!typedPage) {
        navigation.navigationService.page = [[CFRuntime instance] getPageWithId:pageName];
    } else {
        navigation.navigationService.page = typedPage.page;
    }
    navigation.typedPage = typedPage;
    [navigation updateAppearance];
}


#pragma mark DefaultNavigationImplResultDelegate

- (void) navigationCompleted:(DefaultNavigationImpl *)navigation withResult:(id)result {
    if ([self respondsToSelector:@selector(onResultReceived:)]) {
        [self onResultReceived:result];
    }
}

#pragma mark Navigation

- (void) applyHeaderAttributesOnNavigationController:(UINavigationController *)navigationController andButtonTarget:(id<ButtonTarget>)target
{
    [self.navigationImpl delegateParentPageHeaderAttributes:self.navigationService.page.pageHeader toNavigationController:navigationController andButtonTarget:target];
    [DefaultNavigationImpl applyPageHeaderAttributes:self.navigationService.page.pageHeader toNavigationController:navigationController andButtonTarget:target];
}

@end
