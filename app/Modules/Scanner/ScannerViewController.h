//
//  ScannerViewController.h
//  app
//
//  Created by Bernhard Kunnert on 07.01.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class ScannerViewController;

@protocol ScannerViewControllerDelegate <NSObject>

- (void) scanner:(ScannerViewController*) viewController didCaptureCode:(NSString*) code;

@end

@interface ScannerViewController : BaseViewController

@property (weak, nonatomic) id<ScannerViewControllerDelegate> delegate;

@end
