//
//  ExternalContentViewController.m
//  app
//
//  Created by Jakob Haider on 15/09/15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "ExternalContentViewController.h"
#import "CFShare.h"
#import <WebKit/WKWebView.h>

@interface ExternalContentViewController ()

@property (weak, nonatomic) IBOutlet WKWebView *webView;

@end

@implementation ExternalContentViewController

// *********************************************
// * LIEFCYCLE
// *********************************************
#pragma mark -
#pragma mark Lifecycle



- (void)viewDidLoad {
    [super viewDidLoad];

    [self initBarButtons];
    [self initWebView];


    //if it is url link
    if ([self.showUrl isEqual:@"true"]) {
    
      [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.apple.com"]];

    }

    //if it is html file
    if ([self.showHtmlFile isEqual:@"true"]) {

        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.htmlFileName ofType:@"html" inDirectory:@"www"]];
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];

    }

    //if it is html strng
    if ([self.showHtmlString isEqual:@"true"]) {

        //Load html string
        NSString *embedHTML = self.htmlString;

        [self.webView loadHTMLString: embedHTML baseURL: nil];

    }
}

- (void)initBarButtons
{

    
    UIBarButtonItem* btnClose = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(onDismissClicked:)];
    UIBarButtonItem* btnShare = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(onShareClicked:)];

    //check is html pop up
    if ([self.showHtmlFile isEqual:@"true"] || [self.showHtmlString isEqual:@"true"]) {

        [self.navigationItem setRightBarButtonItems:@[btnClose]];

    }else{

//        UIBarButtonItem* btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back_white"] style:UIBarButtonItemStylePlain target:self action:@selector(onBackClicked:)];
//        [self.navigationItem setLeftBarButtonItem:btnBack animated:YES];
        [self.navigationItem setRightBarButtonItems:@[btnClose, btnShare]];
    }
    [self updateBackButton];
}

- (void)initWebView
{
    self.webView.navigationDelegate = self;
}

- (void)onDismissClicked:(UIBarButtonItem*)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)onShareClicked:(UIBarButtonItem*)sender
{
     [CFShare shareString:[self.url absoluteString] parentViewController:self];
}

- (void)onBackClicked:(UIBarButtonItem*)sender
{
    if([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

- (void)updateBackButton
{
    [self.navigationItem.leftBarButtonItem setEnabled:[self.webView canGoBack]];
}

// *********************************************
// * WEBVIEW DELEGATE
// *********************************************
- (void)webViewDidStartLoad:(WKWebView *)webView
{
  //  [self startWaitingAnimation];
}
- (void)webViewDidFinishLoad:(WKWebView *)webView
{
    [self updateBackButton];
   // [self stopWaitingAnimaiton];
}
- (void)webView:(WKWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self updateBackButton];
//    [self stopWaitingAnimaiton];
    
    NSLog(@"Failed to load with error :%@",[error debugDescription]);

}



@end
