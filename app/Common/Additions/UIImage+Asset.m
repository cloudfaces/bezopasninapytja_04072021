//
//  UIImage+Asset.m
//  app
//
//  Created by Bernhard Kunnert on 11.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "UIImage+Asset.h"

@implementation UIImage (Asset)

+ (UIImage*) imageFromAsset:(NSString*) imageName
{
    if (!imageName) {
        return nil;
    }
    
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    
    NSString* imageExtension = [imageName pathExtension];
    NSString* imageFileName = [imageName stringByDeletingPathExtension];
    
    NSString *contentDataPath = [CFRuntime pathForExtractedAppContent];
    
    UIImage* image;
    
    if (screenScale > 1) {
        NSString* hiResPath = [contentDataPath stringByAppendingPathComponent:[[imageFileName stringByAppendingString:@"@2x"] stringByAppendingPathExtension:imageExtension]];
        NSData* hiResImgData = [NSData dataWithContentsOfFile:hiResPath];
        if (hiResImgData) {
            image = [UIImage imageWithData:hiResImgData scale:screenScale];
        }
    }
    
    if (!image) {
        NSString* filePath = [contentDataPath stringByAppendingPathComponent:imageName];
        NSData* imgData = [NSData dataWithContentsOfFile:filePath];
        image = [UIImage imageWithData:imgData scale:1];
    }
    
    return image;
}

+ (UIImage*) navBarImageFromAsset:(NSString*) imageName
{
    if (!imageName) {
        return nil;
    }
    
    UIImage* image;
    if ([PlatformUtil isIos7]) {
        NSString* extension = [imageName pathExtension];
        NSString* iOS7fileName = [imageName stringByDeletingPathExtension];
        iOS7fileName = [iOS7fileName stringByAppendingString:@"_7"];
        if (![NSString isEmpty:extension]) {
            iOS7fileName = [[iOS7fileName stringByAppendingString:@"."] stringByAppendingString:extension];
        }
        
        image = [UIImage imageFromAsset:iOS7fileName];
    }
    
    if (!image) {
        image = [UIImage imageFromAsset:imageName];
    }
    
    return  image;
}

@end
