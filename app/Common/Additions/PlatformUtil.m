//
//  PlatformUtil.m
//  Synthesa
//
//  Created by Bernhard Kunnert on 14.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "PlatformUtil.h"

@implementation PlatformUtil

+ (BOOL) isIos7 {
    return [[UIDevice currentDevice].systemVersion floatValue] >= 7.0;
}

@end
