//
//  UIViewController+Additions.h
//  app
//
//  Created by Simon Moser on 13.10.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecentListViewController.h"
#import "CFPage.h"

@interface UIViewController (Additions) <AppDownloadViewControllerDelegate>
+ (UIViewController *)sharedInstance;
- (void) initRuntimeFromUrl:(NSURL*) url;
- (void) showAppDownloadViewController;
@end
