//
//  UIImage+Average.h
//  app
//
//  Created by Bernhard Kunnert on 26.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Average)

- (UIColor *)averageColor;
- (UIColor *)mergedColor;

@end
