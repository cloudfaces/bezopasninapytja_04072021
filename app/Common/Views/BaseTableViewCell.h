//
//  BaseTableViewCell.h
//  app
//
//  Created by Simon Moser on 31.07.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

@end
