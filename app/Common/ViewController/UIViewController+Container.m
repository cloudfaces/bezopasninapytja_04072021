//
//  UIViewController+Container.m
//  app
//
//  Created by Bernhard Kunnert on 07.01.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "UIViewController+Container.h"

@implementation UIViewController (Container)

- (BOOL) isContainer
{
    return NO;
}

@end
