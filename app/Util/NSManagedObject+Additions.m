//
//  NSManagedObject+Additions.m
//  mobile-pocket
//
//  Created by Simon Moser on 18.11.11.
//  Copyright (c) 2011 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "NSManagedObject+Additions.h"

@implementation NSManagedObject (Additions)

+ (void) deleteAll
{
    NSArray* entities = [self findAll];
    for(int i = 0; i < entities.count; i++) {
        NSManagedObject* object = [entities objectAtIndex:i];
        [object deleteEntity];
    }
    [[NSManagedObject managedObjectContext] save:nil];

}

@end
