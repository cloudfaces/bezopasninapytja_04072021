//
//  mobile_pocketTests.m
//  mobile-pocketTests
//
//  Created by Simon Moser on 30.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "appTests.h"

@implementation appTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExampleFail
{
    STFail(@"Unit tests are not implemented yet in appTests");
}

- (void)testExamplePass
{
    STAssertNotNil(@"not nil", @"should be not nil");
}

@end
